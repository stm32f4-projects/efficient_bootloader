/**
  ******************************************************************************
  * @file    EEPROM_Emulation/src/eeprom.c
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    10-October-2011
  * @brief   This file provides all the EEPROM emulation firmware functions.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */

/** @addtogroup EEPROM_Emulation
  * @{
  */

/* Includes ------------------------------------------------------------------*/
#include "eeprom.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* Global variable used to store variable value in read sequence */
uint16_t DataVar = 0;

/* Virtual address defined by the user: 0xFFFF value is prohibited */
//uint16_t VirtAddVarTab[NB_OF_VAR] =  {0x5555, 0x6666, 0x7777};

const uint16_t VirtAddVarTab[VIRTUAL_ADDR_COUNT] =
{
		0xFF01,  0xFF02,  0xFF03,  0xFF04,  0xFF05,  0xFF06,  0xFF07,  0xFF08,
		0xFF09,  0xFF0A,  0xFF0B,  0xFF0C,  0xFF0D,  0xFF0E,  0xFF0F,  0xFF10,
		0xFF11,  0xFF12,  0xFF13,  0xFF14,  0xFF15,  0xFF16,  0xFF17,  0xFF18,
		0xFF19,  0xFF1A,  0xFF1B,  0xFF1C,  0xFF1D,  0xFF1E,  0xFF1F,  0xFF20,
		0xFF21,  0xFF22,  0xFF23,  0xFF24,  0xFF25,  0xFF26,  0xFF27,  0xFF28,
		0xFF29,  0xFF2A,  0xFF2B,  0xFF2C,  0xFF2D,  0xFF2E,  0xFF2F,  0xFF30,
		0xFF31,  0xFF32,  0xFF33,  0xFF34,	0xFF35,  0xFF36,  0xFF37,  0xFF38,
		0xFF39,  0xFF3A,  0xFF3B,  0xFF3C,	0xFF3D,  0xFF3E,  0xFF3F,  0xFF40,
		0xFF41,  0xFF42,  0xFF43,  0xFF44,	0xFF45,  0xFF46,  0xFF47,  0xFF48,
		0xFF49,  0xFF4A,  0xFF4B,  0xFF4C,	0xFF4D,  0xFF4E,  0xFF4F,  0xFF50,
		0xFF51,  0xFF52,  0xFF53,  0xFF54,	0xFF55,  0xFF56,  0xFF57,  0xFF58,
		0xFF59,  0xFF5A,  0xFF5B,  0xFF5C,	0xFF5D,  0xFF5E,  0xFF5F,  0xFF60,
		0xFF61,  0xFF62,  0xFF63,  0xFF64,	0xFF65,  0xFF66,  0xFF67,  0xFF68,
		0xFF69,  0xFF6A,  0xFF6B,  0xFF6C,	0xFF6D,  0xFF6E,  0xFF6F,  0xFF70,
		0xFF71,  0xFF72,  0xFF73,  0xFF74,	0xFF75,  0xFF76,  0xFF77,  0xFF78,
		0xFF79,  0xFF7A,  0xFF7B,  0xFF7C,  0xFF7D,  0xFF7E,  0xFF7F,  0xFF80,
		0xFF81,  0xFF82,  0xFF83,  0xFF84,  0xFF85,  0xFF86,  0xFF87,  0xFF88,
		0xFF89,  0xFF8A,  0xFF8B,  0xFF8C,  0xFF8D,  0xFF8E,  0xFF8F,  0xFF90,
		0xFF91,  0xFF92,  0xFF93,  0xFF94,  0xFF95,  0xFF96,  0xFF97,  0xFF98,
		0xFF99,  0xFF9A,  0xFF9B,  0xFF9C,  0xFF9D,  0xFF9E,  0xFF9F,  0xFFA0,
		0xFFA1,  0xFFA2,  0xFFA3,  0xFFA4,  0xFFA5,  0xFFA6,  0xFFA7,  0xFFA8,
		0xFFA9,  0xFFAA,  0xFFAB,  0xFFAC,  0xFFAD,  0xFFAE,  0xFFAF,  0xFFB0,
		0xFFB1,  0xFFB2,  0xFFB3,  0xFFB4,  0xFFB5,  0xFFB6,  0xFFB7,  0xFFB8,
		0xFFB9,  0xFFBA,  0xFFBB,  0xFFBC
};

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
static EE_Status EE_Format(void);
static uint16_t EE_FindValidPage(uint8_t Operation);
static uint16_t EE_VerifyPageFullWriteVariable(uint16_t VirtAddress, uint16_t Data);
static uint16_t EE_PageTransfer(uint16_t VirtAddress, uint16_t Data);

/**
  * @brief  Returns the FLASH Status.
  * @param  None
  * @retval FLASH Status: The returned value can be: FLASH_BUSY, FLASH_ERROR_PROGRAM,
  *                       FLASH_ERROR_WRP, FLASH_ERROR_OPERATION or EEPROM_COMPLETE.
  */
EE_Status EE_GetStatus(void)
{
  EE_Status ee_status = EE_COMPLETE;

  if((FLASH->SR & FLASH_FLAG_BSY) == FLASH_FLAG_BSY)
  {
    ee_status = EE_BUSY;
  }
  else
  {
    if((FLASH->SR & FLASH_FLAG_WRPERR) != (uint32_t)0x00)
    {
      ee_status = EE_ERROR_WRP;
    }
    else
    {
      if((FLASH->SR & (uint32_t)0xEF) != (uint32_t)0x00)
      {
        ee_status = EE_ERROR_PROGRAM;
      }
      else
      {
        if((FLASH->SR & FLASH_FLAG_OPERR) != (uint32_t)0x00)
        {
          ee_status = EE_ERROR_OPERATION;
        }
        else
        {
          ee_status = EE_COMPLETE;
        }
      }
    }
  }
  /* Return the FLASH Status */
  return ee_status;
}

/**
  * @brief  Waits for a FLASH operation to complete.
  * @param  None
  * @retval FLASH Status: The returned value can be: FLASH_BUSY, FLASH_ERROR_PROGRAM,
  *                       FLASH_ERROR_WRP, FLASH_ERROR_OPERATION or EEPROM_COMPLETE.
  */
EE_Status EEPROM_WaitForLastOperation(void)
{
  __IO EE_Status status = EE_COMPLETE;

  /* Check for the FLASH Status */
  status = EE_GetStatus();

  /* Wait for the FLASH operation to complete by polling on BUSY flag to be reset.
     Even if the FLASH operation fails, the BUSY flag will be reset and an error
     flag will be set */
  while(status == EE_BUSY)
  {
    status = EE_GetStatus();
  }
  /* Return the operation status */
  return status;
}

/**
  * @brief  Erases a specified FLASH Sector.
  *
  * @param  FLASH_Sector: The Sector number to be erased.
  *          This parameter can be a value between FLASH_Sector_0 and FLASH_Sector_11
  *
  * @param  FLASH_VOLTAGE_RANGE: The device voltage range which defines the erase parallelism.
  *          This parameter can be one of the following values:
  *            @arg FLASH_VOLTAGE_RANGE_1: when the device voltage range is 1.8V to 2.1V,
  *                                  the operation will be done by byte (8-bit)
  *            @arg FLASH_VOLTAGE_RANGE_2: when the device voltage range is 2.1V to 2.7V,
  *                                  the operation will be done by half word (16-bit)
  *            @arg FLASH_VOLTAGE_RANGE_3: when the device voltage range is 2.7V to 3.6V,
  *                                  the operation will be done by word (32-bit)
  *            @arg FLASH_VOLTAGE_RANGE_4: when the device voltage range is 2.7V to 3.6V + External Vpp,
  *                                  the operation will be done by double word (64-bit)
  *
  * @retval FLASH Status: The returned value can be: FLASH_BUSY, FLASH_ERROR_PROGRAM,
  *                       FLASH_ERROR_WRP, FLASH_ERROR_OPERATION or EEPROM_COMPLETE.
  */
EE_Status EEPROM_EraseSector(uint32_t FLASH_Sector, uint8_t FLASH_VOLTAGE_RANGE)
{
  uint32_t tmp_psize = 0x0;
  EE_Status status = EE_COMPLETE;

  /* Check the parameters */
  assert_param(IS_FLASH_SECTOR(FLASH_Sector));
  assert_param(IS_FLASH_VOLTAGE_RANGE(FLASH_VOLTAGE_RANGE));

  if(FLASH_VOLTAGE_RANGE == FLASH_VOLTAGE_RANGE_1)
  {
     tmp_psize = FLASH_PSIZE_BYTE;
  }
  else if(FLASH_VOLTAGE_RANGE == FLASH_VOLTAGE_RANGE_2)
  {
    tmp_psize = FLASH_PSIZE_HALF_WORD;
  }
  else if(FLASH_VOLTAGE_RANGE == FLASH_VOLTAGE_RANGE_3)
  {
    tmp_psize = FLASH_PSIZE_WORD;
  }
  else
  {
    tmp_psize = FLASH_PSIZE_DOUBLE_WORD;
  }
  /* Wait for last operation to be completed */
  status = EEPROM_WaitForLastOperation();

  if(status == EE_COMPLETE)
  {
	    /* If the previous operation is completed, proceed to erase the sector */
	    CLEAR_BIT(FLASH->CR, FLASH_CR_PSIZE);
	    FLASH->CR |= tmp_psize;
	    CLEAR_BIT(FLASH->CR, FLASH_CR_SNB);
	    FLASH->CR |= FLASH_CR_SER | (FLASH_Sector << FLASH_CR_SNB_Pos);
	    FLASH->CR |= FLASH_CR_STRT;

	    /* Wait for last operation to be completed */
	    status = EEPROM_WaitForLastOperation();

	    /* if the erase operation is completed, disable the SER Bit */
	    FLASH->CR &= (~FLASH_CR_SER);
	    FLASH->CR &= SECTOR_MASK;
  }
  /* Return the Erase Status */
  return status;
}

/**
  * @brief  Programs a half word (16-bit) at a specified address.
  * @note   This function must be used when the device voltage range is from 2.1V to 3.6V.
  * @param  Address: specifies the address to be programmed.
  *         This parameter can be any address in Program memory zone or in OTP zone.
  * @param  Data: specifies the data to be programmed.
  * @retval FLASH Status: The returned value can be: FLASH_BUSY, FLASH_ERROR_PROGRAM,
  *                       FLASH_ERROR_WRP, FLASH_ERROR_OPERATION or EEPROM_COMPLETE.
  */
EE_Status EEPROM_ProgramHalfWord(uint32_t Address, uint16_t Data)
{
  EE_Status status = EE_COMPLETE;

  /* Check the parameters */
  assert_param(IS_FLASH_ADDRESS(Address));

  /* Wait for last operation to be completed */
  status = EEPROM_WaitForLastOperation();

  if(status == EE_COMPLETE)
  {
    /* if the previous operation is completed, proceed to program the new data */
    FLASH->CR &= CR_PSIZE_MASK;
    FLASH->CR |= FLASH_PSIZE_HALF_WORD;
    FLASH->CR |= FLASH_CR_PG;

    *(__IO uint16_t*)Address = Data;

    /* Wait for last operation to be completed */
    status = EEPROM_WaitForLastOperation();

    /* if the program operation is completed, disable the PG Bit */
    FLASH->CR &= (~FLASH_CR_PG);
  }
  /* Return the Program Status */
  return status;
}

/**
  * @brief  Restore the pages to a known good state in case of page's status
  *   corruption after a power loss.
  * @param  None.
  * @retval - Flash error code: on write Flash error
  *         - EEPROM_COMPLETE: on success
  */
uint16_t EE_Init(void)
{
  uint16_t PageStatus0 = 6, PageStatus1 = 6;
  uint16_t VarIdx = 0;
  uint16_t EepromStatus = 0, ReadStatus = 0;
  int16_t x = -1;
  uint16_t  ee_status;

  /* Get Page0 status */
  PageStatus0 = (*(__IO uint16_t*)PAGE0_BASE_ADDRESS);
  /* Get Page1 status */
  PageStatus1 = (*(__IO uint16_t*)PAGE1_BASE_ADDRESS);

  /* Check for invalid header states and repair if necessary */
  switch (PageStatus0)
  {
    case ERASED:
      if (PageStatus1 == VALID_PAGE) /* Page0 erased, Page1 valid */
      {
        /* Erase Page0 */
        ee_status = EEPROM_EraseSector(PAGE0_ID,VOLTAGE_RANGE);
        /* If erase operation was failed, a Flash error code is returned */
        if (ee_status != EE_COMPLETE)
          return ee_status;
      }
      else if (PageStatus1 == RECEIVE_DATA) /* Page0 erased, Page1 receive */
      {
        /* Erase Page0 */
        ee_status = EEPROM_EraseSector(PAGE0_ID, VOLTAGE_RANGE);
        /* If erase operation was failed, a Flash error code is returned */
        if (ee_status != EE_COMPLETE)
          return ee_status;

        /* Mark Page1 as valid */
        ee_status = EEPROM_ProgramHalfWord(PAGE1_BASE_ADDRESS, VALID_PAGE);
        /* If program operation was failed, a Flash error code is returned */
        if (ee_status != EE_COMPLETE)
          return ee_status;
      }
      else /* First EEPROM access (Page0&1 are erased) or invalid state -> format EEPROM */
      {
        /* Erase both Page0 and Page1 and set Page0 as valid page */
        ee_status = EE_Format();
        /* If erase/program operation was failed, a Flash error code is returned */
        if (ee_status != EE_COMPLETE)
          return ee_status;
      }
      break;

    case RECEIVE_DATA:
      if (PageStatus1 == VALID_PAGE) /* Page0 receive, Page1 valid */
      {
        /* Transfer data from Page1 to Page0 */
        for (VarIdx = 0; VarIdx < NB_OF_VAR; VarIdx++)
        {
          if (( *(__IO uint16_t*)(PAGE0_BASE_ADDRESS + 6)) == VirtAddVarTab[VarIdx])
            x = VarIdx;

          if (VarIdx != x)
          {
            /* Read the last variables' updates */
            ReadStatus = EE_ReadVariable(VirtAddVarTab[VarIdx], &DataVar);
            /* In case variable corresponding to the virtual address was found */
            if (ReadStatus != 0x1)
            {
              /* Transfer the variable to the Page0 */
              EepromStatus = EE_VerifyPageFullWriteVariable(VirtAddVarTab[VarIdx], DataVar);
              /* If program operation was failed, a Flash error code is returned */
              if (EepromStatus != EE_COMPLETE)
                return EepromStatus;
            }
          }
        }
        /* Mark Page0 as valid */
        ee_status = EEPROM_ProgramHalfWord(PAGE0_BASE_ADDRESS, VALID_PAGE);
        /* If program operation was failed, a Flash error code is returned */
        if (ee_status != EE_COMPLETE)
          return ee_status;

        /* Erase Page1 */
        ee_status = EEPROM_EraseSector(PAGE1_ID, VOLTAGE_RANGE);
        /* If erase operation was failed, a Flash error code is returned */
        if (ee_status != EE_COMPLETE)
          return ee_status;
      }
      else if (PageStatus1 == ERASED) /* Page0 receive, Page1 erased */
      {
        /* Erase Page1 */
        ee_status = EEPROM_EraseSector(PAGE1_ID, VOLTAGE_RANGE);
        /* If erase operation was failed, a Flash error code is returned */
        if (ee_status != EE_COMPLETE)
          return ee_status;
        /* Mark Page0 as valid */
        ee_status = EEPROM_ProgramHalfWord(PAGE0_BASE_ADDRESS, VALID_PAGE);
        /* If program operation was failed, a Flash error code is returned */
        if (ee_status != EE_COMPLETE)
          return ee_status;
      }
      else /* Invalid state -> format eeprom */
      {
        /* Erase both Page0 and Page1 and set Page0 as valid page */
        ee_status = EE_Format();
        /* If erase/program operation was failed, a Flash error code is returned */
        if (ee_status != EE_COMPLETE)
          return ee_status;
      }
      break;

    case VALID_PAGE:
      if (PageStatus1 == VALID_PAGE) /* Invalid state -> format eeprom */
      {
        /* Erase both Page0 and Page1 and set Page0 as valid page */
        ee_status = EE_Format();
        /* If erase/program operation was failed, a Flash error code is returned */
        if (ee_status != EE_COMPLETE)
        {
          return ee_status;
        }
      }
      else if (PageStatus1 == ERASED) /* Page0 valid, Page1 erased */
      {
        /* Erase Page1 */
        ee_status = EEPROM_EraseSector(PAGE1_ID, VOLTAGE_RANGE);
        /* If erase operation was failed, a Flash error code is returned */
        if (ee_status != EE_COMPLETE)
        {
          return ee_status;
        }
      }
      else /* Page0 valid, Page1 receive */
      {
        /* Transfer data from Page0 to Page1 */
        for (VarIdx = 0; VarIdx < NB_OF_VAR; VarIdx++)
        {
          if ((*(__IO uint16_t*)(PAGE1_BASE_ADDRESS + 6)) == VirtAddVarTab[VarIdx])
          {
            x = VarIdx;
          }
          if (VarIdx != x)
          {
            /* Read the last variables' updates */
            ReadStatus = EE_ReadVariable(VirtAddVarTab[VarIdx], &DataVar);
            /* In case variable corresponding to the virtual address was found */
            if (ReadStatus != 0x1)
            {
              /* Transfer the variable to the Page1 */
              EepromStatus = EE_VerifyPageFullWriteVariable(VirtAddVarTab[VarIdx], DataVar);
              /* If program operation was failed, a Flash error code is returned */
              if (EepromStatus != EE_COMPLETE)
              {
                return EepromStatus;
              }
            }
          }
        }
        /* Mark Page1 as valid */
        ee_status = EEPROM_ProgramHalfWord(PAGE1_BASE_ADDRESS, VALID_PAGE);
        /* If program operation was failed, a Flash error code is returned */
        if (ee_status != EE_COMPLETE)
        {
          return ee_status;
        }
        /* Erase Page0 */
        ee_status = EEPROM_EraseSector(PAGE0_ID, VOLTAGE_RANGE);
        /* If erase operation was failed, a Flash error code is returned */
        if (ee_status != EE_COMPLETE)
        {
          return ee_status;
        }
      }
      break;

    default:  /* Any other state -> format eeprom */
      /* Erase both Page0 and Page1 and set Page0 as valid page */
      ee_status = EE_Format();
      /* If erase/program operation was failed, a Flash error code is returned */
      if (ee_status != EE_COMPLETE)
        return ee_status;
      break;
  }

  return EE_COMPLETE;
}

/**
  * @brief  Returns the last stored variable data, if found, which correspond to
  *   the passed virtual address
  * @param  VirtAddress: Variable virtual address
  * @param  Data: Global variable contains the read variable value
  * @retval Success or error status:
  *           - 0: if variable was found
  *           - 1: if the variable was not found
  *           - NO_VALID_PAGE: if no valid page was found.
  */
uint16_t EE_ReadVariable(uint16_t VirtAddress, uint16_t* Data)
{
  uint16_t ValidPage = PAGE0;
  uint16_t AddressValue = VirtAddVarTab[0], ReadStatus = 1;
  uint32_t Address = EEPROM_START_ADDRESS, PageStartAddress = EEPROM_START_ADDRESS;

  /* Get active Page for read operation */
  ValidPage = EE_FindValidPage(READ_FROM_VALID_PAGE);

  /* Check if there is no valid page */
  if (ValidPage == NO_VALID_PAGE)
  {
    return  NO_VALID_PAGE;
  }

  /* Get the valid Page start Address */
  PageStartAddress = (uint32_t)(EEPROM_START_ADDRESS + (uint32_t)(ValidPage * PAGE_SIZE));

  /* Get the valid Page end Address */
  Address = (uint32_t)((EEPROM_START_ADDRESS - 2) + (uint32_t)((1 + ValidPage) * PAGE_SIZE));

  /* Check each active page address starting from end */
  while (Address > (PageStartAddress + 2))
  {
    /* Get the current location content to be compared with virtual address */
    AddressValue = (*(__IO uint16_t*)Address);

    /* Compare the read address with the virtual address */
    if (AddressValue == VirtAddress)
    {
      /* Get content of Address-2 which is variable value */
      *Data = (*(__IO uint16_t*)(Address - 2));

      /* In case variable value is read, reset ReadStatus flag */
      ReadStatus = 0;

      break;
    }
    else
    {
      /* Next address location */
      Address = Address - 4;
    }
  }

  /* Return ReadStatus value: (0: variable exist, 1: variable doesn't exist) */
  return ReadStatus;
}

/**
  * @brief  Writes/upadtes variable data in EEPROM.
  * @param  VirtAddress: Variable virtual address
  * @param  Data: 16 bit data to be written
  * @retval Success or error status:
  *           - EEPROM_COMPLETE: on success
  *           - PAGE_FULL: if valid page is full
  *           - NO_VALID_PAGE: if no valid page was found
  *           - Flash error code: on write Flash error
  */
uint16_t EE_WriteVariable(uint16_t VirtAddress, uint16_t Data)
{
  uint16_t Status = 0;

  /* Write the variable virtual address and value in the EEPROM */
  Status = EE_VerifyPageFullWriteVariable(VirtAddress, Data);

  /* In case the EEPROM active page is full */
  if (Status == PAGE_FULL)
  {
    /* Perform Page transfer */
    Status = EE_PageTransfer(VirtAddress, Data);
  }

  /* Return last operation status */
  return Status;
}

/**
  * @brief  Erases PAGE and PAGE1 and writes VALID_PAGE header to PAGE
  * @param  None
  * @retval Status of the last operation (Flash write or erase) done during
  *         EEPROM formating
  */
static EE_Status EE_Format(void)
{
  EE_Status ee_status = EE_COMPLETE;

  /* Erase Page0 */
  ee_status = EEPROM_EraseSector(PAGE0_ID, VOLTAGE_RANGE);

  /* If erase operation was failed, a Flash error code is returned */
  if (ee_status != EE_COMPLETE)
  {
    return ee_status;
  }

  /* Set Page0 as valid page: Write VALID_PAGE at Page0 base address */
  ee_status = EEPROM_ProgramHalfWord(PAGE0_BASE_ADDRESS, VALID_PAGE);

  /* If program operation was failed, a Flash error code is returned */
  if (ee_status != EE_COMPLETE)
  {
    return ee_status;
  }

  /* Erase Page1 */
  ee_status = EEPROM_EraseSector(PAGE1_ID, VOLTAGE_RANGE);

  /* Return Page1 erase operation status */
  return ee_status;
}

/**
  * @brief  Find valid Page for write or read operation
  * @param  Operation: operation to achieve on the valid page.
  *   This parameter can be one of the following values:
  *     @arg READ_FROM_VALID_PAGE: read operation from valid page
  *     @arg WRITE_IN_VALID_PAGE: write operation from valid page
  * @retval Valid page number (PAGE or PAGE1) or NO_VALID_PAGE in case
  *   of no valid page was found
  */
static uint16_t EE_FindValidPage(uint8_t Operation)
{
  uint16_t PageStatus0 = 6, PageStatus1 = 6;

  /* Get Page0 actual status */
  PageStatus0 = (*(__IO uint16_t*)PAGE0_BASE_ADDRESS);

  /* Get Page1 actual status */
  PageStatus1 = (*(__IO uint16_t*)PAGE1_BASE_ADDRESS);

  /* Write or read operation */
  switch (Operation)
  {
    case WRITE_IN_VALID_PAGE:   /* ---- Write operation ---- */
      if (PageStatus1 == VALID_PAGE)
      {
        /* Page0 receiving data */
        if (PageStatus0 == RECEIVE_DATA)
        {
          return PAGE0;         /* Page0 valid */
        }
        else
        {
          return PAGE1;         /* Page1 valid */
        }
      }
      else if (PageStatus0 == VALID_PAGE)
      {
        /* Page1 receiving data */
        if (PageStatus1 == RECEIVE_DATA)
        {
          return PAGE1;         /* Page1 valid */
        }
        else
        {
          return PAGE0;         /* Page0 valid */
        }
      }
      else
      {
        return NO_VALID_PAGE;   /* No valid Page */
      }

    case READ_FROM_VALID_PAGE:  /* ---- Read operation ---- */
      if (PageStatus0 == VALID_PAGE)
      {
        return PAGE0;           /* Page0 valid */
      }
      else if (PageStatus1 == VALID_PAGE)
      {
        return PAGE1;           /* Page1 valid */
      }
      else
      {
        return NO_VALID_PAGE ;  /* No valid Page */
      }

    default:
      return PAGE0;             /* Page0 valid */
  }
}

/**
  * @brief  Verify if active page is full and Writes variable in EEPROM.
  * @param  VirtAddress: 16 bit virtual address of the variable
  * @param  Data: 16 bit data to be written as variable value
  * @retval Success or error status:
  *           - EEPROM_COMPLETE: on success
  *           - PAGE_FULL: if valid page is full
  *           - NO_VALID_PAGE: if no valid page was found
  *           - Flash error code: on write Flash error
  */
static uint16_t EE_VerifyPageFullWriteVariable(uint16_t VirtAddress, uint16_t Data)
{
  EE_Status ee_status = EE_COMPLETE;
  uint16_t ValidPage = PAGE0;
  uint32_t Address = EEPROM_START_ADDRESS, PageEndAddress = EEPROM_START_ADDRESS+PAGE_SIZE;

  /* Get valid Page for write operation */
  ValidPage = EE_FindValidPage(WRITE_IN_VALID_PAGE);

  /* Check if there is no valid page */
  if (ValidPage == NO_VALID_PAGE)
  {
    return  NO_VALID_PAGE;
  }

  /* Get the valid Page start Address */
  Address = (uint32_t)(EEPROM_START_ADDRESS + (uint32_t)(ValidPage * PAGE_SIZE));

  /* Get the valid Page end Address */
  PageEndAddress = (uint32_t)((EEPROM_START_ADDRESS - 2) + (uint32_t)((1 + ValidPage) * PAGE_SIZE));

  /* Check each active page address starting from begining */
  while (Address < PageEndAddress)
  {
    /* Verify if Address and Address+2 contents are 0xFFFFFFFF */
    if ((*(__IO uint32_t*)Address) == 0xFFFFFFFF)
    {
      /* Set variable data */
      ee_status = EEPROM_ProgramHalfWord(Address, Data);
      /* If program operation was failed, a Flash error code is returned */
      if (ee_status != EE_COMPLETE)
      {
        return ee_status;
      }
      /* Set variable virtual address */
      ee_status = EEPROM_ProgramHalfWord(Address + 2, VirtAddress);
      /* Return program operation status */
      return ee_status;
    }
    else
    {
      /* Next address location */
      Address = Address + 4;
    }
  }

  /* Return PAGE_FULL in case the valid page is full */
  return PAGE_FULL;
}

/**
  * @brief  Transfers last updated variables data from the full Page to
  *   an empty one.
  * @param  VirtAddress: 16 bit virtual address of the variable
  * @param  Data: 16 bit data to be written as variable value
  * @retval Success or error status:
  *           - EEPROM_COMPLETE: on success
  *           - PAGE_FULL: if valid page is full
  *           - NO_VALID_PAGE: if no valid page was found
  *           - Flash error code: on write Flash error
  */
static uint16_t EE_PageTransfer(uint16_t VirtAddress, uint16_t Data)
{
  EE_Status ee_status = EE_COMPLETE;
  uint32_t NewPageAddress = EEPROM_START_ADDRESS;
  uint16_t OldPageId=0;
  uint16_t ValidPage = PAGE0, VarIdx = 0;
  uint16_t EepromStatus = 0, ReadStatus = 0;

  /* Get active Page for read operation */
  ValidPage = EE_FindValidPage(READ_FROM_VALID_PAGE);

  if (ValidPage == PAGE1)       /* Page1 valid */
  {
    /* New page address where variable will be moved to */
    NewPageAddress = PAGE0_BASE_ADDRESS;

    /* Old page ID where variable will be taken from */
    OldPageId = PAGE1_ID;
  }
  else if (ValidPage == PAGE0)  /* Page0 valid */
  {
    /* New page address  where variable will be moved to */
    NewPageAddress = PAGE1_BASE_ADDRESS;

    /* Old page ID where variable will be taken from */
    OldPageId = PAGE0_ID;
  }
  else
  {
    return NO_VALID_PAGE;       /* No valid Page */
  }

  /* Set the new Page status to RECEIVE_DATA status */
  ee_status = EEPROM_ProgramHalfWord(NewPageAddress, RECEIVE_DATA);
  /* If program operation was failed, a Flash error code is returned */
  if (ee_status != EE_COMPLETE)
  {
    return ee_status;
  }

  /* Write the variable passed as parameter in the new active page */
  EepromStatus = EE_VerifyPageFullWriteVariable(VirtAddress, Data);
  /* If program operation was failed, a Flash error code is returned */
  if (EepromStatus != EE_COMPLETE)
  {
    return EepromStatus;
  }

  /* Transfer process: transfer variables from old to the new active page */
  for (VarIdx = 0; VarIdx < NB_OF_VAR; VarIdx++)
  {
    if (VirtAddVarTab[VarIdx] != VirtAddress)  /* Check each variable except the one passed as parameter */
    {
      /* Read the other last variable updates */
      ReadStatus = EE_ReadVariable(VirtAddVarTab[VarIdx], &DataVar);
      /* In case variable corresponding to the virtual address was found */
      if (ReadStatus != 0x1)
      {
        /* Transfer the variable to the new active page */
        EepromStatus = EE_VerifyPageFullWriteVariable(VirtAddVarTab[VarIdx], DataVar);
        /* If program operation was failed, a Flash error code is returned */
        if (EepromStatus != EE_COMPLETE)
        {
          return EepromStatus;
        }
      }
    }
  }

  /* Erase the old Page: Set old Page status to ERASED status */
  ee_status = EEPROM_EraseSector(OldPageId, VOLTAGE_RANGE);
  /* If erase operation was failed, a Flash error code is returned */
  if (ee_status != EE_COMPLETE)
  {
    return ee_status;
  }

  /* Set new Page status to VALID_PAGE status */
  ee_status = EEPROM_ProgramHalfWord(NewPageAddress, VALID_PAGE);
  /* If program operation was failed, a Flash error code is returned */
  if (ee_status != EE_COMPLETE)
  {
    return ee_status;
  }

  /* Return last operation flash status */
  return ee_status;
}

/**
  * @}
  */

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
