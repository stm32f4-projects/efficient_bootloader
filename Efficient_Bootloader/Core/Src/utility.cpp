/*
 * sort.cpp
 *
 *  Created on: Nov 2, 2021
 *      Author: ugur
 */

#include <algorithm>
#include <vector>
#include <map>
#include <stdint.h>

extern "C" uint32_t bestIndexForNonOverwritableMemory(uint8_t* arr, const uint32_t len, const uint8_t blockCountNeeded, void (*eraser)(void) );
extern "C" void flashReadData (uint32_t StartPageAddress, uint32_t *RxBuf, uint16_t numberofwords);

/**
* @brief  Read flash memory data function
* @param  	StartPageAddress : Address to start reading
* @param  	RxBuf : Read buffer
* @retval	None
*/
void flashReadData (uint32_t StartPageAddress, uint32_t *RxBuf, uint16_t numberofwords)
{
	while (1) {
		*RxBuf = *(volatile uint32_t *)StartPageAddress;
		StartPageAddress += 4;
		RxBuf++;
		if (!(numberofwords--)) break;
	}
}

/**
* @brief  Returns whether the specified array has sequential values equal to the number of length.
* @param  	arr : Point of array
* @param  	len : Length of array
* @retval		bool
*/
bool isSequentialArray(const uint8_t* array, const uint32_t len)
{
	if(!len)
		return (array[0] == array[1]);

	for (uint32_t i = 0; i < len-1; ++i) {
		if (array[i] != array[i + 1])
			return false;
	}
	return true;
}

/**
* @brief  Returns whether the specified array has sequential values less than or
* equal to the number of length.
* @param  	arr : Point of array
* @param  	len : Length of array
* @retval		bool
*/
bool isSequentialArrayLessOrEqual(const uint32_t* array, const uint32_t len)
{
	for (uint32_t i = 0; i < len - 1; ++i) {
		if (array[i] < array[i + 1])
			return false;
	}
	return true;
}

/**
* @brief  Returns the address of the maximum value in the specified array.
* @param  	arr : Point of array
* @param  	len : Length of array
* @retval		uint32_t*
*/
uint32_t* getMaxValAddr(uint32_t* array, const uint32_t len)
{
	const auto ret = std::max_element(array, array + len);
	return ret;
}

/**
* @brief  Returns the address of the minimum value in the specified array.
* @param  	arr : Point of array
* @param  	len : Length of array
* @retval		uint32_t*
*/
template<typename T>
T* getMinValAddr(T* array, const uint32_t len)
{
	const auto ret = std::min_element(array, array + len);
	return ret;
}

/**
* @brief  Returns the next address of the minimum value in the specified array.
* @param  	arr : Point of array
* @param  	len : Length of array
* @param  	increasedArr :  An array whose minimum value is equal to the next
* minimum value of the specified array.
* @retval		uint32_t*
*/
uint32_t getNextMinValIndex(uint32_t* arr, const uint32_t len, const uint32_t num, uint32_t* increasedArr)
{
	if (!increasedArr)
		return 0;

	std::copy_n(arr, len, increasedArr);
	uint32_t i = 0;
	uint32_t j = 0;

	const auto maxVal = *getMaxValAddr(arr, len);

	for (j = 0; j < num; ++j) {
		auto pminArr = getMinValAddr(increasedArr, len);
		if (*pminArr >= maxVal) {
			return pminArr - increasedArr;
		}
		const auto val = *pminArr;

		for (i = 0; i < len; ++i) {
			if (increasedArr[i] == val)
				increasedArr[i] = 0xFFFF;
		}

	}
	const auto nextMinAddr = getMinValAddr(increasedArr, len);
	const auto nextMinIndex = nextMinAddr - increasedArr;

	return nextMinIndex;
}

/**
* @brief  Increments the value of all elements in the specified array by one.
* @param  	arr : Point of array
* @param  	len : Length of array
* @retval		None
*/
void increaseFlashWriteCount(uint32_t* arr, const uint32_t len)
{
	for (uint32_t i = 0; i < len; ++i)
		arr[i]++;
}

/**
* @brief  Decrements the value of all elements in the specified array by one.
* @param  	arr : Point of array
* @param  	len : Length of array
* @retval		None
*/
void decreaseFlashWriteCount(uint32_t* arr, const uint32_t len)
{
	for (uint32_t i = 0; i < len; ++i)
		arr[i]--;
}

/**
* @brief  Returns the number of elements with the minimum value in the specified array.
* @param  	arr : Point of array
* @param  	len : Length of array
* @retval		uint32
*/
uint32_t getMinCount(uint32_t* arr, const uint32_t len)
{
	auto minVal = getMinValAddr(arr, len);
	auto count = 0;

	for (uint32_t i = 0; i < len; ++i) {
		if (arr[i] == *minVal)
			count++;
	}

	return count;
}

/**
* @brief Returns the number of elements with the specified value in the specified array.
* @param  	arr : Point of array
* @param  	len : Length of array
* @retval		uint32
*/
uint32_t getValCount(const uint32_t* arr, const uint32_t len, const uint32_t val)
{
	auto count = 0;
	for (uint32_t i = 0; i < len; ++i)
		if (arr[i] == val)
			count++;
	return count;
}

/**
* @brief  Returns how many sequential values are in the specified number
* according to non-overwritable memory
* @param  	arr : Point of array
* @param  	len : Length of array
* @param  	val	: Expected value to be sequential
* @param  	valCount : Number of values that should be in sequential
* @param  	indexArr : Index numbers of sequential values
* @retval		uint32_t
*/
uint32_t getSequentialCountForNonOverWritableMemory(const uint8_t* arr, const uint32_t len, const uint32_t val, const uint32_t valCount)
{
	auto cnt = 0;
	for (uint32_t i = 0; i < len; ++i)
		if (arr[i] == val && isSequentialArray(&arr[i], valCount))
			cnt++;

	return cnt;
}

/**
* @brief  Returns howgetSequentialCountForNonOverWritableMemory many sequential values are in the specified number.
* @param  	arr : Point of array
* @param  	len : Length of array
* @param  	val	: Expected value to be sequential
* @param  	valCount : Number of values that should be in sequential
* @param  	indexArr : Index numbers of sequential values
* @retval		uint32_t
*/
uint32_t getSequentialCountForOverWritableMemory(const uint32_t* arr, const uint32_t len, const uint32_t val, const uint32_t valCount, std::vector<uint32_t>& indexArr)
{
	for (uint32_t i = 0; i < len; ++i) {
		if (arr[i] == val) {
			if (isSequentialArrayLessOrEqual(&arr[i], valCount))
				indexArr.push_back(i);
		}
	}
	return indexArr.size();
}

/**
* @brief Returns max value of multimap
* @param 	multiMap :
* @return 	x : max value iterator of multimap
*/
auto getMaxValMultimap(std::multimap<uint32_t, int>& multiMap)
{
	int minCountDiffVal = multiMap.begin()->second;
	auto x = std::max_element(multiMap.begin(), multiMap.end(),
		[&minCountDiffVal](const std::pair<uint32_t, int>& p1, const std::pair<uint32_t, int>& p2) {
			if (minCountDiffVal < p2.second) {
				minCountDiffVal = p2.second;
				return true;
			}
			return false;
		});

	return x;
}

/**
 * @brief Returns the optimal index number in the specified array
 * @param  	arr : Point of array
 * @param  	len : Length of array
 * @param 	blockCountNeeded : Number of sectors required
 * @return 	uint32_t : best index
 */
uint32_t bestIndexForOverwritableMemory(uint32_t* arr, const uint32_t len, const uint8_t blockCountNeeded)
{
	auto* tempArr = (uint32_t*)malloc(sizeof(uint32_t) * len);
	if (!tempArr)
		return 0;

	uint32_t bestIndex = getNextMinValIndex(arr, len, 0, tempArr);

	std::vector<uint32_t> indexArr;

	const auto maxVal	= *getMaxValAddr(arr, len);
	const auto minCount = getMinCount(arr, len);

	std::multimap<uint32_t, int> valCountDiff;	//TODO <i,j>
	std::multimap<uint32_t, int> minCountDiff;	//TODO <i,j>

	for (uint32_t i = 0; i < len; i++) {		//TODO en fazla min ve min+1'e baksin
		const auto selectedMinIndex = getNextMinValIndex(arr, len, i, tempArr);
		if (const auto sequentialCount = getSequentialCountForOverWritableMemory(arr, len, arr[selectedMinIndex], blockCountNeeded, indexArr)) {
			std::copy_n(arr, len, tempArr);
			for (uint32_t j = 0; j < sequentialCount; ++j){
				const auto valCount1 = getValCount(tempArr, len, arr[indexArr[j]]);

				increaseFlashWriteCount(&tempArr[indexArr[j]], blockCountNeeded);

				const auto valCount2 = getValCount(tempArr, len, arr[indexArr[j]]);
				const auto tempMinCount = getMinCount(tempArr, len);

				int diffVal = static_cast<int>(minCount - tempMinCount);
				if (diffVal < 0)
					diffVal = 0;
				minCountDiff.insert(std::make_pair(i, static_cast<uint32_t>(diffVal)));

				diffVal = static_cast<int>(valCount1 - valCount2);
				if (diffVal < 0)
					diffVal = 0;
				valCountDiff.insert(std::make_pair(i, static_cast<uint32_t>(diffVal)));

				decreaseFlashWriteCount(&tempArr[indexArr[j]], blockCountNeeded);
			}
			break;
		}

		if (arr[selectedMinIndex] >= maxVal) {
			return bestIndex;
		}
	}

	//TODO min Count oncelikli. O yuzden ilk onu karsilastir
	const auto minCountMap = getMaxValMultimap(minCountDiff);

	if (const auto minCountDist = std::distance(minCountDiff.begin(), minCountMap)) {
		bestIndex = indexArr[minCountDist];
	}else{
		const auto valCountMap  = getMaxValMultimap(valCountDiff);
		const auto valCountDist = std::distance(valCountDiff.begin(), valCountMap);
		bestIndex = indexArr[valCountDist];
	}

	free(tempArr);
	return bestIndex;
}

/**
 * @brief Returns the optimal index number in the specified array
 * @param  	arr : Point of array
 * @param  	len : Length of array
 * @param 	blockCountNeeded : Number of sectors required
 * @param 	eraser : function pointer for erase memory blocks
 * @return 	uint32_t : best index
 */
uint32_t bestIndexForNonOverwritableMemory(uint8_t* arr, const uint32_t len, const uint8_t blockCountNeeded, void (*eraser)(void) )
{
	auto* minValAddr = getMinValAddr(arr, len);
	uint32_t index =  minValAddr - arr ;

	//sequence var mi kontrol et
	if(!getSequentialCountForNonOverWritableMemory(arr, len, *minValAddr, blockCountNeeded)){
		//sequence  yoksa eraseOverlapIfNeeded cagir
		eraser();
		return 0xDEADBEEF;
	}

	//bulunan min'den itibaren sigacak sekilde yaz
//	while(blockCountNeeded != (len - index)){
//		minValAddr--;
//		index =  minValAddr - arr ;
//	}

	return index;
}

