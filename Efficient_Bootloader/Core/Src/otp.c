/*
 * otp.c
 *
 *  Created on: May 15, 2022
 *      Author: ugur
 */

#include "otp.h"

#define OTP_FLASH_TIMEOUT_VALUE       5000U /* 5 s */

HAL_StatusTypeDef OTPWrite(uint8_t block, uint8_t byteIndex, uint8_t data)
{
	HAL_StatusTypeDef status;

	/* Check input parameters */
	if (block >= OTP_BLOCKS || byteIndex >= OTP_BYTES_IN_BLOCK )
		return HAL_ERROR;

	HAL_FLASH_Unlock();

	status = FLASH_WaitForLastOperation((uint32_t)OTP_FLASH_TIMEOUT_VALUE);

	if (status != HAL_OK) {
		HAL_FLASH_Lock();
		return status;
	}

	status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE,
							   OTP_START_ADDR + block * OTP_BYTES_IN_BLOCK + byteIndex,
							   data );

	HAL_FLASH_Lock();

	return status;
}

uint8_t OTPRead(uint8_t block, uint8_t byteIndex)
{
	uint8_t data;

	/* Check input parameters */
	if (block >= OTP_BLOCKS || byteIndex >= OTP_BYTES_IN_BLOCK )
		return 0;

	/* Get value */
	data = *(__IO uint8_t *)(OTP_START_ADDR + block * OTP_BYTES_IN_BLOCK + byteIndex);

	/* Return data */
	return data;
}

HAL_StatusTypeDef TM_OTP_BlockLock(uint8_t block)
{
	HAL_StatusTypeDef status;

	/* Check input parameters */
	if (block >= OTP_BLOCKS)
		return HAL_ERROR;

	/* Unlock FLASH */
	HAL_FLASH_Unlock();

	status = FLASH_WaitForLastOperation((uint32_t)OTP_FLASH_TIMEOUT_VALUE);

	if (status != HAL_OK) {
		HAL_FLASH_Lock();
		return HAL_ERROR;
	}

	/* Write byte */
	status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE,
							   OTP_LOCK_ADDR + block,
							   0x00);

	HAL_FLASH_Lock();

	return status;
}
