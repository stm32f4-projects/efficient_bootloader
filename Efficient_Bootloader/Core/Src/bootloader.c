/*
 * bootloader.c
 *
 *  Created on: Jul 27, 2021
 *      Author: ugur
 */
#include <stdbool.h>

#include "bootloader.h"
#include "main.h"
#include "eeprom.h"
#include "otp.h"

/*************************Extern Variables*******************************/
extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart3;
extern CRC_HandleTypeDef hcrc;

/*************************Define Variables*******************************/
#define BL_DEBUG_MSG_EN 1
#define DEBUG_UART 	&huart3
#define COM_UART 	&huart2

/*Version Info*/
#define BUILD 	0
#define MINOR	0
#define MAJOR	1

/************************Global Variables*******************************/
uint8_t supportedCommands[] = {
		BL_GET_VER,
		BL_GET_HELP,
		BL_GET_CID,
		BL_GET_RDP_STATUS	,
		BL_GO_TO_ADDR	,
		BL_FLASH_ERASE ,
		BL_MEM_WRITE,
		BL_EN_RW_PROTECT,
		BL_DIS_R_W_PROTECT,
		BL_READ_SECTOR_P_STATUS,
		BL_OTP_READ,
		BL_MEM_READ,
		BL_GET_BOOT_INFO,
		BL_GET_FLASH_START_ADDR
};

uint8_t versionInfo[] = {
		BUILD,
		MINOR,
		MAJOR
};

static const uint32_t memoryBlockAddress[MEMORY_BLOCK_COUNT] = {
    0x08010000, 0x08014000, 0x08018000, 0x0801C000, 0x08020000, 0x08024000,
    0x08028000, 0x0802C000, 0x08030000, 0x08034000, 0x08038000, 0x0803C000,
    0x08040000, 0x08044000, 0x08048000, 0x0804C000, 0x08050000, 0x08054000,
    0x08058000, 0x0805C000, 0x08060000, 0x08064000, 0x08068000, 0x0806C000,
    0x08070000, 0x08074000, 0x08078000, 0x0807C000, 0x08080000, 0x08084000,
    0x08088000, 0x0808C000, 0x08090000, 0x08094000, 0x08098000, 0x0809C000,
    0x080A0000, 0x080A4000, 0x080A8000, 0x080AC000, 0x080B0000, 0x080B4000,
    0x080B8000, 0x080BC000, 0x080C0000, 0x080C4000, 0x080C8000, 0x080CC000,
    0x080D0000, 0x080D4000, 0x080D8000, 0x080DC000, 0x080E0000, 0x080E4000,
    0x080E8000, 0x080EC000, 0x080F0000, 0x080F4000, 0x080F8000, 0x080FC000};

static const uint8_t sectorBlocksIndexArray[] =
{
	SECTOR_4_BLOCK_INDEX,
	SECTOR_5_BLOCK_INDEX,
	SECTOR_6_BLOCK_INDEX,
	SECTOR_7_BLOCK_INDEX,
	SECTOR_8_BLOCK_INDEX,
	SECTOR_9_BLOCK_INDEX,
	SECTOR_10_BLOCK_INDEX,
	SECTOR_11_BLOCK_INDEX
};

BootInfo_t bootInfo_t = {0};

uint8_t blRxBuffer[BL_RX_BUFF_LEN];

/**********************Function Prototypes*****************************/
uint8_t executeFlashErase(uint8_t sectorName , uint8_t eraseblockCount, uint32_t* sectorError);
void flashReadData (uint32_t StartPageAddress, uint32_t *RxBuf, uint16_t numberofwords);
uint32_t bestIndexForNonOverwritableMemory(uint8_t* arr, const uint32_t len, const uint8_t blockCountNeeded, void (*eraser)(void) );
/**********************************************************************/

/**********************Function Definitions****************************/

int getEmptyMemoryBlockCount(uint8_t *blocks) {
    uint32_t data[2] = {0};
    uint8_t count = 0;

    HAL_FLASH_Unlock();

    for (uint8_t i = 0; i < MEMORY_BLOCK_COUNT; ++i) {
        flashReadData(memoryBlockAddress[i], data, 1);
        if (*data == BOOT_INFO_FLASH_EMPTY_VAL) {
            blocks[i] = 0;
            count++;
        } else {
            blocks[i] = 1;
        }
    }

    HAL_FLASH_Lock();

    return count;
}

/*
 * memory block index'in hangi sectore ait oldugunu dondur
 * */
int findSectorfromMemoryBlock(uint8_t blockNum)
{
	if(blockNum>=SECTOR_4_BLOCK_INDEX && blockNum < SECTOR_5_BLOCK_INDEX)
		return FLASH_SECTOR_4;
	else if(blockNum>=SECTOR_5_BLOCK_INDEX && blockNum < SECTOR_6_BLOCK_INDEX)
		return FLASH_SECTOR_5;
	else if(blockNum>=SECTOR_6_BLOCK_INDEX && blockNum < SECTOR_7_BLOCK_INDEX)
		return FLASH_SECTOR_6;
	else if(blockNum>=SECTOR_7_BLOCK_INDEX && blockNum < SECTOR_8_BLOCK_INDEX)
		return FLASH_SECTOR_7;
	else if(blockNum>=SECTOR_8_BLOCK_INDEX && blockNum < SECTOR_9_BLOCK_INDEX)
		return FLASH_SECTOR_8;
	else if(blockNum>=SECTOR_9_BLOCK_INDEX && blockNum < SECTOR_10_BLOCK_INDEX)
		return FLASH_SECTOR_9;
	else if(blockNum>=SECTOR_10_BLOCK_INDEX && blockNum < SECTOR_11_BLOCK_INDEX)
		return FLASH_SECTOR_10;
	else if(blockNum>=SECTOR_11_BLOCK_INDEX && blockNum < MEMORY_BLOCK_COUNT)
			return FLASH_SECTOR_11;
	else
		return -1;
}

/*
 * Sector'lerin baslangic memory block index'ini dondur
 * */
int findStartMemoryBlockOfSector(uint8_t sector)
{
	if(sector == 4)
		return 0;
	else if(sector == 5)
		return SECTOR_5_BLOCK_INDEX;
	else if(sector == 6)
		return SECTOR_6_BLOCK_INDEX;
	else if(sector == 7)
		return SECTOR_7_BLOCK_INDEX;
	else if(sector == 8)
		return SECTOR_8_BLOCK_INDEX;
	else if(sector == 9)
		return SECTOR_9_BLOCK_INDEX;
	else if(sector == 10)
		return SECTOR_10_BLOCK_INDEX;
	else if(sector == 11)
		return SECTOR_11_BLOCK_INDEX;
	else
		return -1;
}

/*
 * Verilen indekse gore bir sonraki sector baslangic adresini dondurur
 * */
uint8_t getNextSectorBlockIndex(uint8_t idx)
{
	for(int i = 0; i < sizeof(sectorBlocksIndexArray) - 1 ; ++i){
		if (idx >= sectorBlocksIndexArray[i] && idx < sectorBlocksIndexArray[i + 1])
			return sectorBlocksIndexArray[i + 1];
	}

	return sectorBlocksIndexArray[SECTOR_11_BLOCK_INDEX];
}

/*
 *Sector erase isleminden sonra memory block erasure  count'lari sifirla
 * */
void resetErasureCountAfterErase(uint8_t sector)
{
	const uint8_t startMemoryBlock = findStartMemoryBlockOfSector(sector);
	const uint8_t endMemoryBlock = findStartMemoryBlockOfSector(sector+1);

	for(int i = 0; i< endMemoryBlock - startMemoryBlock; ++i)
		bootInfo_t.memoryBlockErasureCount[startMemoryBlock + i] = 0;

	updateBootInfo();
}

/*
 * Eger index'te write count 0 dan buyukse, 0 dan buyuk sectorleri sil
 * Erasure Count lari guncelle
 * tekrar best index bul
 * */
void eraseOverlapIfNeeded(void)
{
	uint32_t sectorError = 0;
	for(int idx = 0; idx < MEMORY_BLOCK_COUNT; ++idx){
		if (!bootInfo_t.memoryBlockErasureCount[idx]){
            uint8_t sector =
                findSectorfromMemoryBlock(idx); // block numarasindan sector bul

            uint8_t result = executeFlashErase(sector, 1, &sectorError);       // belirlenen sectoru sil
            if(result != HAL_OK){
				printMsg(BL_ERROR"Flash Erase Operation Failed!"
						"\r\nSector Error = %#x\r\n",sectorError);
            }

            idx = getNextSectorBlockIndex(idx) - 1;
        }
    }
}

void printMsg(char* format, ...)
{
#ifdef BL_DEBUG_MSG_EN
	char str[120];

	/*Extract the argument list using VA APIs*/
	va_list args;
	va_start(args, format);
	vsprintf(str, format, args);
	HAL_UART_Transmit(DEBUG_UART, (uint8_t*)str, strlen(str), HAL_MAX_DELAY);
	va_end(args);

#endif
}

/*code to jump user application*/
void bootloaderJumpUserApp(void)
{
	printMsg(BL_INFO " bootloaderJumpUserApp\r\n");

	/*set MSP*/
	uint32_t mspValue = *(volatile uint32_t*)bootInfo_t.currentMemoryBlockAddr;
	__set_MSP(mspValue);

	/*set reset handler*/
	void (*appResetHandler)(void);
	uint32_t resetHandlerAddr = *(volatile uint32_t*)(bootInfo_t.currentMemoryBlockAddr + 4);
	appResetHandler = (void*)resetHandlerAddr;

	printMsg(BL_INFO "Jump Address = %#x\r\n", mspValue);
	printMsg(BL_INFO "Reset Address = %#x \r\n", resetHandlerAddr);

	/*jump to reset handler of user app*/
	appResetHandler();

}

void routeProgramFlow(void)
{
	if(HAL_GPIO_ReadPin(B1_GPIO_Port, B1_Pin) == GPIO_PIN_SET){
		printMsg(BL_INFO "Button is pressed... Going to BL Mode\r\n");
		bootloaderReadData();
	} else{
		printMsg(BL_INFO "Button is not pressed... Going to User App Mode\r\n");
		bootloaderJumpUserApp();
	}
}

void  BLSendACK(uint16_t len)
{
	uint8_t ackBuff[4];
	ackBuff[0] = BL_ACK_1;
	ackBuff[1] = BL_ACK_2;
	ackBuff[2] = len & 0xFF;
	ackBuff[3] = (len >> 8) & 0xFF;
	HAL_UART_Transmit(COM_UART, ackBuff, 4, HAL_MAX_DELAY);
}

void  BLSendNACK(void)
{
    uint8_t nack = BL_NACK;
    HAL_UART_Transmit(COM_UART, &nack, 1, HAL_MAX_DELAY);
}

uint8_t BLVerifyCRC(uint8_t* pData, uint32_t len, uint32_t crcHost)
{
	uint32_t uwCRCValue = 0xFF;

	for (uint32_t i = 0;  i < len; ++ i) {
		uint32_t iData = pData[i];
		uwCRCValue = HAL_CRC_Accumulate(&hcrc, &iData, 1);
	}
	/* Reset CRC Calculation Unit */
	  __HAL_CRC_DR_RESET(&hcrc);

	if(uwCRCValue == crcHost)
		return VERIFY_CRC_SUCCESS;

	return VERIFY_CRC_FAIL;
}

//Read the chip identifier or device Identifier
uint16_t getMCUChipId(void)
{
/*
	The STM32F4xx MCUs integrate an MCU ID code. This ID identifies the ST MCU part-
number and the die revision. It is part of the DBG_MCU component and is mapped on the
external PPB bus (see Section 38.16 on page 1690). This code is accessible using the
JTAG debug port (4 to 5 pins) or the SW debug port (two pins) or by the user software. It is
even accessible while the MCU is under system reset.*/
	uint16_t cid;
	cid = (uint16_t)(DBGMCU->IDCODE) & 0x0FFF;
	return  cid;

}

//Read Protection Level
uint8_t getFlashRDPLevel(void)
{
	uint8_t status = 0;
#if 0
	FLASH_OBProgramInitTypeDef obHandle;
	HAL_FLASHEx_OBGetConfig(&obHandle);
	status = (uint8_t)obHandle.RDPLevel;
#else
	volatile uint32_t* pOBAddr = (uint32_t*)0x1FFFC000;
	status = (uint8_t)(*pOBAddr >> 8);
#endif
	return status;
}

uint16_t getOBRWProtectionStatus(void)
{
    //This structure is given by ST Flash driver to hold the OB(Option Byte) contents .
	FLASH_OBProgramInitTypeDef OBInit;

	//First unlock the OB(Option Byte) memory access
	HAL_FLASH_OB_Unlock();
	//get the OB configuration details
	HAL_FLASHEx_OBGetConfig(&OBInit);
	//Lock back .
	HAL_FLASH_Lock();

	//We are just interested in r/w protection status of the sectors.
	return (uint16_t)OBInit.WRPSector;

}

uint8_t executeFlashErase(uint8_t sectorName , uint8_t eraseblockCount, uint32_t* sectorError)
{
   //we have totally 12 sectors in STM32F407VG mcu .. sector[0 to 11]
	//number_of_sector has to be in the range of 0 to 11
	// if sector_number = 0xff , that means mass erase !
	//Code needs to modified if your MCU supports more flash sectors
	FLASH_EraseInitTypeDef flashErase_handle;
	HAL_StatusTypeDef status;

	if( eraseblockCount > TOTAL_FLASH_SECTOR)
		eraseblockCount = TOTAL_FLASH_SECTOR;

	if( (sectorName == 0xff ) || (sectorName < TOTAL_FLASH_SECTOR) )
	{
		if(sectorName ==  FLASH_MASS_ERASE_CMD){
			flashErase_handle.TypeErase = FLASH_TYPEERASE_MASSERASE;
			flashErase_handle.Banks = FLASH_BANK_1;
		}else {
		    /*Here we are just calculating how many sectors needs to erased */
			uint8_t remanining_sector = TOTAL_FLASH_SECTOR - sectorName;

			if( eraseblockCount > remanining_sector)
				eraseblockCount = remanining_sector;

			flashErase_handle.TypeErase = FLASH_TYPEERASE_SECTORS;
			flashErase_handle.Sector = sectorName; // this is the initial sector
			flashErase_handle.NbSectors = eraseblockCount;
		}

		/*Get access to touch the flash registers */
		HAL_FLASH_Unlock();
		flashErase_handle.VoltageRange = FLASH_VOLTAGE_RANGE_3;  // our mcu will work on this voltage range
		status = (uint8_t) HAL_FLASHEx_Erase(&flashErase_handle, sectorError);
		HAL_FLASH_Lock();

		for(int i = 0; i<eraseblockCount; ++i)
			resetErasureCountAfterErase(sectorName+i);

		return status;
	}

	return INVALID_SECTOR;
}

/*This function writes the contents of pbuff to  "mem_address" byte by byte */
//Note1 : Currently this function supports writing to Flash only .
//Note2 : This functions does not check whether "mem_address" is a valid address of the flash range.
uint8_t executeFlashWrite(uint8_t *pbuff, uint32_t memAddr, uint32_t len)
{
    //We have to unlock flash module to get control of registers
     uint8_t status = HAL_FLASH_Unlock();
    if(status){
    	printMsg(BL_ERROR "Flash unlock error!\n");
    	return status;
    }

	while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET);

    for(uint32_t i = 0 ; i <len ; i++){
        status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, memAddr + i, pbuff[i] );
    }

	while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET);

    HAL_FLASH_Lock();

    return status;
}

uint32_t executeFlashRead(uint32_t address)
{
	uint32_t data;

	/* Get value */
	data = *(__IO uint32_t *)(address);

	/* Return data */
	return data;
}

/*
 *
 */
void clearCurrentErasureCounts(void) {
    bootInfo_t.currentMemoryBlockAddr = FLASH_SECTOR4_BASE_ADDRESS;
    bootInfo_t.payloadLen = 0;
    int isUpdateNeeded = 0;
    for (int i = 0; i < MEMORY_BLOCK_COUNT; ++i){
        if(bootInfo_t.memoryBlockErasureCount[i]){
        	bootInfo_t.memoryBlockErasureCount[i] = 0;
        	isUpdateNeeded++;
        }
    }

    if(isUpdateNeeded)
    	updateBootInfo();
}

void updateBootInfo(void)
{
	//glow the led to indicate bootloader is currently writing to memory
	HAL_GPIO_WritePin(LD6_GPIO_Port, LD6_Pin, GPIO_PIN_SET);
	HAL_FLASH_Unlock();

	int len = sizeof(bootInfo_t)>>1;	//EE_WriteVariable get uint16_t Data
	for(int i = 0 ; i <len ; i++){
		uint16_t result = EE_WriteVariable(VirtAddVarTab[i], *((uint16_t*)&bootInfo_t + i));
		if(result != EE_COMPLETE){
			printMsg(BL_ERROR "EEPROM Write failed!\r\n");
			break;
		}
	}

	HAL_FLASH_Lock();
	//turn off the led to indicate memory write is over
	HAL_GPIO_WritePin(LD6_GPIO_Port, LD6_Pin, GPIO_PIN_RESET);
}

void readBootInfo(void)
{
	HAL_FLASH_Unlock();

	int len = sizeof(bootInfo_t)>>1;	//EE_WriteVariable get uint16_t Data
	for(int i = 0 ; i <len ; i++){
		uint16_t result = EE_ReadVariable(VirtAddVarTab[i], ((uint16_t *)&bootInfo_t + i));
		if(result){
			printMsg(BL_ERROR "EEPROM Read failed!\r\n");
			break;
		}
	}

     HAL_FLASH_Lock();
}

void initBootInfo(void)
{
	HAL_FLASH_Unlock();

	EE_Init();

    readBootInfo();

    HAL_FLASH_Lock();

    uint8_t block_state[MEMORY_BLOCK_COUNT] = {0};
    bool editNextCycle = false;
    bool incorrectMemoryInfo = false;

    int emptyMemoryCount = getEmptyMemoryBlockCount(block_state);
    printMsg(BL_INFO "Empty Memory Count is : %d\r\n", emptyMemoryCount);

    uint8_t testOTP = 0x31;
    OTPWrite(0,0,testOTP);

    if (!emptyMemoryCount) {
        printMsg(BL_ERROR "All memory blocks are full! Erasing full chip!\r\n");
        uint32_t sectorError = 0;
        uint8_t result = executeFlashErase(FLASH_SECTOR_4, 8, &sectorError);
        if(result != HAL_OK){
        	printMsg(BL_ERROR"Flash Erase Operation Failed!"
        			"\r\nSector Error = %#x\r\n",sectorError);
        	return;
        }
    } else if (emptyMemoryCount == 60) {
        printMsg(BL_INFO "All memory blocks are empty\r\n");
        clearCurrentErasureCounts();
    } else {
        for (int i = 0; i < MEMORY_BLOCK_COUNT; ++i) {
            printMsg(
                BL_INFO "block[%d] = %d | memoryBlockErasureCount[%d] = %d\r\n",
                i, block_state[i], i, bootInfo_t.memoryBlockErasureCount[i]);

            if (block_state[i] != bootInfo_t.memoryBlockErasureCount[i]) {
                printMsg(BL_ERROR "memoryBlockErasureCount is WRONG!\r\n");
                bootInfo_t.memoryBlockErasureCount[i] = block_state[i];
                incorrectMemoryInfo = true;
            }

            if (bootInfo_t.memoryBlockErasureCountHistory[i] == 0 &&
                block_state[i] == 1) {
                printMsg(BL_ERROR
                         "memoryBlockErasureCountHistory is WRONG!\r\n");
                bootInfo_t.memoryBlockErasureCountHistory[i] = 1;
            }

            if (incorrectMemoryInfo) {
                bootInfo_t.payloadLen = 0;
                // 1 cevrim sonra currentMemoryBlockAddr belirlenebilir
                if (editNextCycle) {
                    bootInfo_t.currentMemoryBlockAddr = memoryBlockAddress[i];
                    editNextCycle = false;
                }

                if (block_state[i] &&
                    bootInfo_t.currentMemoryBlockAddr < memoryBlockAddress[i]) {
                    printMsg(BL_ERROR "currentMemoryBlockAddr is WRONG!\r\n");
                    editNextCycle = true;
                }
            }
        }
//        for(int i = 0; i < MEMORY_BLOCK_COUNT; ++i)
//        	bootInfo_t.memoryBlockErasureCountHistory[i] = 14;
        updateBootInfo();
    }
}

/*
Modifying user option bytes
To modify the user option value, follow the sequence below:
1. Check that no Flash memory operation is ongoing by checking the BSY bit in the
FLASH_SR register
2. Write the desired option value in the FLASH_OPTCR register.
3. Set the option start bit (OPTSTRT) in the FLASH_OPTCR register
4. Wait for the BSY bit to be cleared.
*/
static uint8_t configureFlashSectorRWProtection(uint16_t sector_details, uint8_t protection_mode, uint8_t disable)
{
    //First configure the protection mode
    //protection_mode =1 , means write protect of the user flash sectors

	 //Flash option control register (OPTCR)
	volatile uint32_t *pOPTCR = (uint32_t*) 0x40023C14;

	//disable all r/w protection on sectors
	if(disable) {

		//Option byte configuration unlock
		HAL_FLASH_OB_Unlock();

		//wait till no active operation on flash
		while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET);

		//clear the 31st bit (default state)
		//please refer : Flash option control register (FLASH_OPTCR) in RM
		*pOPTCR &= ~(1 << 31);

		//clear the protection : make all bits belonging to sectors as 1
		*pOPTCR |= (0xFFF << 16);

		//Set the option start bit (OPTSTRT) in the FLASH_OPTCR register
		*pOPTCR |= ( 1 << 1);

		//wait till no active operation on flash
		while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET);

		HAL_FLASH_OB_Lock();
		return 1;
		}

	if(protection_mode == (uint8_t) 1) {
		//we are putting write protection on the sectors encoded in sector_details argument

		//Option byte configuration unlock
		HAL_FLASH_OB_Unlock();

		//wait till no active operation on flash
		while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET);

		//here we are setting just write protection for the sectors
		//clear the 31st bit
		//please refer : Flash option control register (FLASH_OPTCR) in RM
		*pOPTCR &= ~(1 << 31);

		//Dual-bank on 1 Mbyte Flash memory devices
		//we are setting single bank Flash memory (contiguous addresses in bank1)
		*pOPTCR &= ~(1 << 30);

		//put write protection on sectors
		*pOPTCR &= ~ (sector_details << 16);

		//Set the option start bit (OPTSTRT) in the FLASH_OPTCR register
		*pOPTCR |= ( 1 << 1);

		//wait till no active operation on flash
		while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET);

		HAL_FLASH_OB_Lock();
	} else{
    	printMsg(BL_ERROR "Wrong protection code!\r\n");
    	return 0;
    }
	return 1;
}

static int isReadyToHandleFunc(uint8_t* pbuff, uint16_t ACK_len)
{
    printMsg(BL_INFO "Checksum control...\r\n");

    // Total length of the command packet
    uint32_t commandPacketLength = (pbuff[0] | (pbuff[1]<<8)) + 1;
    uint32_t hostCRC = *((uint32_t *)(pbuff + commandPacketLength - 4));

    if (BLVerifyCRC(pbuff, commandPacketLength - CRC_LENGTH, hostCRC)) {
        printMsg(BL_INFO "checksum success !!\r\n");
        BLSendACK(ACK_len);
        return 1;
    }
    return 0;
}

static void CRCFail(void)
{
	printMsg(BL_ERROR " CRC Failed!\r\n");
	BLSendNACK();
}

//verify the address sent by the host .
static uint8_t verifyAddress(uint32_t goAddress)
{
	//so, what are the valid addresses to which we can jump ?
	//can we jump to system memory ? yes
	//can we jump to sram1 memory ?  yes
	//can we jump to sram2 memory ? yes
	//can we jump to backup sram memory ? yes
	//can we jump to peripheral memory ? its possible , but dont allow. so no
	//can we jump to external memory ? yes.

//incomplete -poorly written .. optimize it
	if ( goAddress >= SRAM1_BASE && goAddress <= SRAM1_END)
		return ADDR_VALID;
	else if ( goAddress >= SRAM2_BASE && goAddress <= SRAM2_END)
		return ADDR_VALID;
	else if ( goAddress >= FLASH_BASE && goAddress <= FLASH_END)
		return ADDR_VALID;
	else if ( goAddress >= BKPSRAM_BASE && goAddress <= BKPSRAM_END)
		return ADDR_VALID;
	else
		return ADDR_INVALID;
}

static uint32_t determineAddressforEndurance(uint32_t memAddr, uint8_t blockCount)
{
	if(memAddr == ASSIGN_AUTO_ADDRESS_CMD){
        uint32_t index = bestIndexForNonOverwritableMemory(
            bootInfo_t.memoryBlockErasureCount, MEMORY_BLOCK_COUNT, blockCount,
            eraseOverlapIfNeeded);

        if (index == 0xDEADBEEF)
            return 0;

        printMsg(BL_INFO "Best index = %d\r\n", index);

        bootInfo_t.currentMemoryBlockAddr = memoryBlockAddress[index];
		for(uint32_t i = 0; i<blockCount; ++i){
			bootInfo_t.memoryBlockErasureCount[index + i]++;
			bootInfo_t.memoryBlockErasureCountHistory[index + i]++;
		}

		return memoryBlockAddress[index];
	}
	return memAddr;
}

static HAL_StatusTypeDef BLUartWriteData(uint8_t* pbuff, uint32_t len)
{
	return HAL_UART_Transmit(COM_UART, pbuff, len, HAL_MAX_DELAY);
}

void bootloaderReadData(void)
{
	HAL_GPIO_WritePin(LD5_GPIO_Port, LD5_Pin, 1);

	while(1){
		memset(blRxBuffer, 0, BL_RX_BUFF_LEN);

		HAL_UART_Receive(COM_UART, blRxBuffer, 2, HAL_MAX_DELAY);
		HAL_UART_Receive(COM_UART, &blRxBuffer[2], (blRxBuffer[0] | (blRxBuffer[1]<<8))-1, HAL_MAX_DELAY);

		switch (blRxBuffer[2]) {
		    case BL_GET_VER:
		        BLHandleGetVerCmd(blRxBuffer);
		        break;
		    case BL_GET_HELP:
		        BLHandleGetHelpCmd(blRxBuffer);
		        break;
		    case BL_GET_CID:
		        BLHandleGetCidCmd(blRxBuffer);
		        break;
		    case BL_GET_RDP_STATUS:
		        BLHandleGetRdpCmd(blRxBuffer);
		        break;
		    case BL_GO_TO_ADDR:
		        BLHandleGoCmd(blRxBuffer);
		        break;
		    case BL_FLASH_ERASE:
		        BLHandleFlashEraseCmd(blRxBuffer);
		        break;
		    case BL_MEM_WRITE:
		        BLHandleMemWriteCmd(blRxBuffer);
		        break;
		    case BL_EN_RW_PROTECT:
		        BLHandleEnRWProtect(blRxBuffer);
		        break;
		    case BL_DIS_R_W_PROTECT:
		        BLHandleDisRWProtect(blRxBuffer);
		        break;
		    case BL_READ_SECTOR_P_STATUS:
		        BLHandleReadSectorProtectionStatus(blRxBuffer);
		        break;
		    case BL_OTP_READ:
		        BLHandleReadOTP(blRxBuffer);
		        break;
		    case BL_MEM_READ:
				BLHandleMemRead(blRxBuffer);
				break;
		    case BL_GET_BOOT_INFO:
				BLHandleGetBootInfo(blRxBuffer);
				break;
		    case BL_GET_FLASH_START_ADDR:
		    	BLHandleGetFlashStartAddr(blRxBuffer);
				break;
		    default:
                printMsg(BL_ERROR
                         "Invalid command code{%#x} received from host \r\n",
                         blRxBuffer[2]);
                break;
            }
    }
}

void BLHandleGetVerCmd(uint8_t* pbuff)
{
	printMsg(BL_INFO " bootloader get version command\r\n");

	if (isReadyToHandleFunc(pbuff, (uint16_t)sizeof(versionInfo)))
		BLUartWriteData(versionInfo, sizeof(versionInfo));
    else
        CRCFail();
}

void BLHandleGetHelpCmd(uint8_t* pbuff)
{
    printMsg(BL_INFO " bootloader get help command\r\n");

    if (isReadyToHandleFunc(pbuff, (uint16_t)sizeof(supportedCommands)))
        BLUartWriteData(supportedCommands, sizeof(supportedCommands));
    else
        CRCFail();
}

void BLHandleGetCidCmd(uint8_t* pbuff)
{
    printMsg(BL_INFO " bootloader get CID command\r\n");

    if (isReadyToHandleFunc(pbuff, 2)) {
        uint16_t cidNum = getMCUChipId();
        printMsg(BL_INFO " CRC MCU ID : %d  %#x !!\r\n", cidNum, cidNum);
        BLUartWriteData((uint8_t *)&cidNum, 2);
    } else {
        CRCFail();
    }
}

void BLHandleGetRdpCmd(uint8_t* pbuff)
{
    printMsg(BL_INFO " bootloader get RDP Level command\r\n");

    if (isReadyToHandleFunc(pbuff, 1)) {
        uint16_t status = getFlashRDPLevel();
        printMsg(BL_INFO " MCU RDP Level : %d  %#x !!\r\n", status, status);
        BLUartWriteData((uint8_t *)&status, 1);
    } else {
        CRCFail();
    }
}

void BLHandleGoCmd(uint8_t* pbuff)
{
    uint8_t addrValid = ADDR_VALID;
    uint8_t addrInvalid = ADDR_INVALID;

    printMsg(BL_INFO "bootloader Go specified Address command\r\n");

    if (isReadyToHandleFunc(pbuff, 1)) {
        // extract the go address
        uint32_t goAddress = *((uint32_t *)&pbuff[3]);
        printMsg(BL_INFO "GO addr: %#x\r\n", goAddress);

        if (verifyAddress(goAddress) == ADDR_VALID) {
            // tell host that address is fine
            BLUartWriteData(&addrValid, 1);

            /*jump to "go" address.
            we dont care what is being done there.
            host must ensure that valid code is present over there
            Its not the duty of bootloader. so just trust and jump */

            /* Not doing the below line will result in hardfault exception for
             * ARM cortex M */
            // watch : https://www.youtube.com/watch?v=VX_12SjnNhY

            if (!(goAddress & 1))
                goAddress += 1; // make T bit =1

            printMsg(BL_INFO " jumping to %#08x address! \r\n", goAddress);

            void (*lets_jump)(void) = (void *)goAddress;
            lets_jump();

        } else {
            printMsg(BL_ERROR "GO addr invalid !\r \n");
            // tell host that address is invalid
            BLUartWriteData(&addrInvalid, 1);
        }
    } else {
        CRCFail();
    }
}

void BLHandleFlashEraseCmd(uint8_t* pbuff)
{
    printMsg(BL_INFO "Bootloader handle flash erase cmd\r\n");

    if (isReadyToHandleFunc(pbuff, 1)) {
        printMsg(BL_INFO "Initial_sector : %d  no_ofsectors: %d\r\n", pbuff[3],
                 pbuff[4]);

        HAL_GPIO_WritePin(LD4_GPIO_Port, LD4_Pin, 1);
        uint32_t sectorError = 0;
        uint8_t result = executeFlashErase(pbuff[3], pbuff[4], &sectorError);

        if(result != HAL_OK){
			printMsg(BL_ERROR"Flash Erase Operation Failed!"
					"\r\nSector Error = %#x\r\n",sectorError);
        }

        HAL_GPIO_WritePin(LD4_GPIO_Port, LD4_Pin, 0);

        printMsg(BL_INFO "Flash erase status: %#x\r\n", result);
        BLUartWriteData(&result, 1);
    } else {
        CRCFail();
    }
}

void BLHandleMemWriteCmd(uint8_t* pbuff)
{
    uint32_t memAddr = *((uint32_t *)(&pbuff[3]));
    uint8_t blockCountNeeded = pbuff[7];
    uint16_t payloadLen = pbuff[8] | (pbuff[9]<<8);
    uint8_t sendCount = pbuff[10];

    if (!blockCountNeeded) {
        printMsg(BL_ERROR "blockCountNeeded could not be zero!\r\n");
        return;
    }

    printMsg(BL_INFO "bootloader_handle_mem_write_cmd\r\n");

    if (isReadyToHandleFunc(pbuff, 1)) {
        if (!sendCount) {
            memAddr = determineAddressforEndurance(memAddr, blockCountNeeded);

            if (memAddr == 0xDEADBEEF) {
                printMsg(BL_ERROR
                         "Determination Address for Endurance failed\r\n");
                uint8_t ret = 0;
                BLUartWriteData(&ret, 1);
                return;
            }

            bootInfo_t.payloadLen = 0;
        } else {
            memAddr = bootInfo_t.currentMemoryBlockAddr + bootInfo_t.payloadLen;
        }
        bootInfo_t.payloadLen += payloadLen;
        printMsg(BL_INFO " mem write address : %#x\r\n", memAddr);

        uint8_t writeStatus = 0;

		updateBootInfo();

        if (verifyAddress(memAddr) == ADDR_VALID) {
            printMsg(BL_INFO " valid mem write address\r\n");

            // glow the led to indicate bootloader is currently writing to
            // memory
            HAL_GPIO_WritePin(LD6_GPIO_Port, LD6_Pin, GPIO_PIN_SET);

            // execute mem write
            writeStatus = executeFlashWrite(&pbuff[11], memAddr, payloadLen);

            // turn off the led to indicate memory write is over
            HAL_GPIO_WritePin(LD6_GPIO_Port, LD6_Pin, GPIO_PIN_RESET);

            // inform host about the status
            BLUartWriteData(&writeStatus, 1);

        } else {
            printMsg(BL_ERROR " Invalid mem write address\r\n");
            writeStatus = ADDR_INVALID;
            // inform host that address is invalid
            BLUartWriteData(&writeStatus, 1);
        }
    } else {
        CRCFail();
    }
}

void BLHandleEnRWProtect(uint8_t* pbuff)
{
    printMsg(BL_INFO "bootloader_handle_endis_rw_protect\r\n");

    if (isReadyToHandleFunc(pbuff, 1)) {
        uint16_t sector_details = pbuff[3] | (pbuff[4] << 8);
        uint8_t status =
            configureFlashSectorRWProtection(sector_details, pbuff[5], 0);

        if (!status) {
            printMsg(BL_ERROR "Enable RW protection status error!!\r\n");
            BLSendNACK();
            return;
        }
        printMsg(BL_INFO " Enable RW protection command successfull\r\n");
        BLUartWriteData(&status, 1);
    } else {
        CRCFail();
    }
}

void BLHandleDisRWProtect(uint8_t* pbuff)
{
    printMsg(BL_INFO "bootloader_handle_dis_rw_protect\r\n");

    if (isReadyToHandleFunc(pbuff, 1)) {
        uint8_t status = configureFlashSectorRWProtection(0, 0, 1);

        if (!status) {
            printMsg(BL_ERROR "Enable RW protection status error!!\r\n");
            BLSendNACK();
            return;
        }
        printMsg(BL_INFO " Disable RW protection command successfull\r\n");
        BLUartWriteData(&status, 1);
    } else {
        CRCFail();
    }
}

void BLHandleReadSectorProtectionStatus(uint8_t* pbuff)
{
    printMsg(BL_INFO "bootloader_handle_read_sector_protection_status\r\n");

    if (isReadyToHandleFunc(pbuff, 2)) {
        uint16_t status = getOBRWProtectionStatus();
        printMsg(BL_INFO "nWRP status: %#x\r\n", status);
        BLUartWriteData((uint8_t *)&status, 2);
    } else {
        CRCFail();
    }
}

void BLHandleReadOTP(uint8_t* pbuff)
{
	printMsg(BL_INFO "bootloader_handle_read_OTP\r\n");

	uint8_t otpBlock = pbuff[3];
	uint8_t otpByteIndex = pbuff[4];

	if (isReadyToHandleFunc(pbuff, 1)) {
		uint8_t otpValue = OTPRead(otpBlock, otpByteIndex);

		printMsg(BL_INFO "OTP Byte from [%d]Block-[%d]Index: %#x\r\n",
				 otpBlock,
				 otpByteIndex,
				 otpValue);

		BLUartWriteData(&otpValue, 1);
	} else {
		CRCFail();
	}
}

void BLHandleMemRead(uint8_t* pbuff)
{
    printMsg(BL_INFO "bootloader_handle_memory_read\r\n");

    uint32_t memAddr = *((uint32_t *)(&pbuff[3]));

    if (isReadyToHandleFunc(pbuff, 4)) {
		uint32_t data = executeFlashRead(memAddr);
		printMsg(BL_INFO "[%#x] : %#x\r\n",memAddr,data);
		BLUartWriteData((uint8_t*)&data, 4);
    } else {
        CRCFail();
    }
}

void BLHandleGetBootInfo(uint8_t* pbuff)
{
	printMsg(BL_INFO " bootloader get boot info command\r\n");

	if(isReadyToHandleFunc(pbuff, (uint16_t)sizeof(bootInfo_t))){
		readBootInfo();
		BLUartWriteData((uint8_t *)&bootInfo_t , sizeof(bootInfo_t));
	}	else{
		CRCFail();
	}
}

void BLHandleGetFlashStartAddr(uint8_t* pbuff)
{
	printMsg(BL_INFO " bootloader process output file command\r\n");

	if(isReadyToHandleFunc(pbuff, 4)){
		uint32_t index = bestIndexForNonOverwritableMemory(bootInfo_t.memoryBlockErasureCount,
														   MEMORY_BLOCK_COUNT,
														   pbuff[3],
														   eraseOverlapIfNeeded);

		uint32_t offset = memoryBlockAddress[index];
		offset -= FLASH_SECTOR0_BASE_ADDRESS;
		if(offset < 0)
			offset = 0;

		printMsg(BL_INFO "Flash address start must %d bytes far away from origin in linker file.", offset);

		BLUartWriteData((uint8_t*)&offset, 4);
	}	else{
			CRCFail();
	}
}



