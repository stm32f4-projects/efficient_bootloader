/*
 * bootloader.h
 *
 *  Created on: Jul 27, 2021
 *      Author: ugur
 */

#ifndef INC_BOOTLOADER_H_
#define INC_BOOTLOADER_H_

#include <stdarg.h>
#include <stdio.h>
#include <string.h>

// clang-format off

#define BL_INFO 	"BL_INFO: "
#define BL_ERROR 	"BL_ERROR: "

typedef enum {
	SECTOR_0_BLOCK_INDEX = -1,
	SECTOR_1_BLOCK_INDEX = -1,
	SECTOR_2_BLOCK_INDEX = -1,
	SECTOR_3_BLOCK_INDEX = -1,
	SECTOR_4_BLOCK_INDEX = 0,
	SECTOR_5_BLOCK_INDEX = 4,
	SECTOR_6_BLOCK_INDEX = 12,
	SECTOR_7_BLOCK_INDEX = 20,
	SECTOR_8_BLOCK_INDEX = 28,
	SECTOR_9_BLOCK_INDEX = 36,
	SECTOR_10_BLOCK_INDEX = 44,
	SECTOR_11_BLOCK_INDEX = 52,
	MEMORY_BLOCK_COUNT = 60,
}sector_block_indexes;


#define FLASH_SECTOR0_BASE_ADDRESS 	0x08000000U
#define FLASH_SECTOR4_BASE_ADDRESS 	0x08010000U
#define BL_RX_BUFF_LEN				2080

//16kbyte * (62+2) block = 1024 kbyte flash memory
//1. and 2. block belong to bootloader
//The remaining 62 memory blocks will be used for flash programming.
//#define MEMORY_BLOCK_COUNT 	60
#define CURRENT_MEMORY_BLOCK_ADDR_INDEX  	((MEMORY_BLOCK_COUNT / 2) + 1)

#define BL_ACK_1  	0xA5
#define BL_ACK_2  	0x5A
#define BL_NACK 	0x7F

/*Bootloaders Commands*/
//This command is used to read the bootloader version from the MCU
#define BL_GET_VER				0x51
//This command is used to know what are the commands supported by the bootloader
#define BL_GET_HELP				0x52
//This command is used to read the MCU chip identification number
#define BL_GET_CID				0x53
//This command is used to read the FLASH Read Protection level.
#define BL_GET_RDP_STATUS		0x54
//This command is used to jump bootloader to specified address.
#define BL_GO_TO_ADDR			0x55
//This command is used to mass erase or sector erase of the user flash .
#define BL_FLASH_ERASE          0x56
//This command is used to write data in to different memories of the MCU
#define BL_MEM_WRITE			0x57
//This command is used to enable or disable read/write protect on different sectors of the user flash .
#define BL_EN_RW_PROTECT 		0x58
//This command is used disable all sector read/write protection
#define BL_DIS_R_W_PROTECT 		0x59
//This command is used to read all the sector protection status.
#define BL_READ_SECTOR_P_STATUS	0x5A
//This command is used to read the OTP contents.
#define BL_OTP_READ 			0x5B
//This command is used to read data from different memories of the microcontroller.
#define BL_MEM_READ 			0x5C
//This command is used to read the boot information.
#define BL_GET_BOOT_INFO 		0x5D
//This command is used to process output file.
#define BL_GET_FLASH_START_ADDR	0x5E

#define ADDR_VALID 				0x01
#define ADDR_INVALID 			0x00

#define INVALID_SECTOR 			0x04
#define TOTAL_FLASH_SECTOR		12

#define FLASH_MASS_ERASE_CMD 	0xFF

/*Some Start and End addresses of different memories of STM32F446xx MCU */
/*Change this according to your MCU */
#define SRAM1_SIZE 	                112 * 1024 // STM32F407VG has 112KB of SRAM1
#define SRAM1_END 	                (SRAM1_BASE + SRAM1_SIZE)
#define SRAM2_SIZE 	                16 * 1024 // STM32F407VG has 16KB of SRAM2
#define SRAM2_END 					(SRAM2_BASE + SRAM2_SIZE)
#define FLASH_SIZE 					1024 * 1024 // STM32F407VG has 1MB of FLASH
#define BKPSRAM_SIZE 				4 * 1024  // STM32F407VG has 4KB of SRAM2
#define BKPSRAM_END 				(BKPSRAM_BASE + BKPSRAM_SIZE)

#define BOOT_INFO_FLASH_EMPTY_VAL 	0xFFFFFFFF
#define ASSIGN_AUTO_ADDRESS_CMD 	0xFFFFFFFF

// clang-format on

typedef struct{
	uint32_t currentMemoryBlockAddr;
	uint32_t payloadLen;
	uint16_t memoryBlockErasureCountHistory[MEMORY_BLOCK_COUNT];
	uint8_t memoryBlockErasureCount[MEMORY_BLOCK_COUNT];
}BootInfo_t;

extern BootInfo_t bootInfo_t;

void printMsg(char* format, ...);
void bootloaderJumpUserApp(void);
void routeProgramFlow(void);
void bootloaderReadData(void);

void BLHandleGetVerCmd(uint8_t* buffer);
void BLHandleGetHelpCmd(uint8_t* buffer);
void BLHandleGetCidCmd(uint8_t* buffer);
void BLHandleGetRdpCmd(uint8_t* buffer);
void BLHandleGoCmd(uint8_t* buffer);
void BLHandleFlashEraseCmd(uint8_t* buffer);
void BLHandleMemWriteCmd(uint8_t* buffer);
void BLHandleEnRWProtect(uint8_t* buffer);
void BLHandleMemRead(uint8_t* buffer);
void BLHandleReadSectorProtectionStatus(uint8_t* buffer);
void BLHandleReadOTP(uint8_t* buffer);
void BLHandleDisRWProtect(uint8_t* buffer);
void BLHandleGetBootInfo(uint8_t* buffer);
void BLHandleGetFlashStartAddr(uint8_t* buffer);

void initBootInfo(void);
void updateBootInfo(void);

#endif /* INC_BOOTLOADER_H_ */
