/*
 * otp.h
 *
 *  Created on: May 15, 2022
 *      Author: ugur
 */

#ifndef INC_OTP_H_
#define INC_OTP_H_

#include "stm32f4xx_hal.h"

/**
 * @brief  OTP memory start address
 */
#define OTP_START_ADDR		(0x1FFF7800)

/**
 * @brief  OTP memory lock address
 */
#define OTP_LOCK_ADDR		(0x1FFF7A00)

/**
 * @brief  Number of OTP blocks
 */
#define OTP_BLOCKS			16

/**
 * @brief  Number of bytes in one block
 */
#define OTP_BYTES_IN_BLOCK	32

/**
 * @brief  Number of all OTP bytes
 */
#define OTP_SIZE			(OTP_BLOCKS * OTP_BYTES_IN_BLOCK)


/**
 * @brief  Writes OTP data to specific block and specific byte index
 * @note   You can only ONCE write data at desired byte in specific block,
 * if you will try to do it more times, you can have broken data at this location.
 * @param  block: OTP block number, 0 to 15 is allowed
 * @param  byteIndex: OTP byte index inside one block, 0 to 31 is allowed
 * @param  data: Data to be written to OTP memory
 * @retval HAL_StatusTypeDef HAL Status
 */
HAL_StatusTypeDef OTPWrite(uint8_t block, uint8_t byteIndex, uint8_t data);

/**
 * @brief  Reads OTP data from specific block and specific byte index
 * @note   You can read data unlimited times from locations
 * @param  block: OTP block number, 0 to 15 is allowed
 * @param  byteIndex: OTP byte index inside one block, 0 to 31 is allowed
 * @retval Value at specific block and byte index location, or 0 if location is invalid
 */
uint8_t OTPRead(uint8_t block, uint8_t byteIndex);

/**
 * @brief  Locks entire block to prevent future programming inside
 * @note   When you lock your block, then you are not able to program it anymore.
 * 	       Even, if it is totally empty. You can't unlock it back!
 * @param  block: OTP block number, 0 to 15 is allowed
 * @retval HAL_StatusTypeDef HAL Status
 */
HAL_StatusTypeDef TM_OTP_BlockLock(uint8_t block);

#endif /* INC_OTP_H_ */
