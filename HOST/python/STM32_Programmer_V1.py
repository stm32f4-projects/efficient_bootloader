from abc import abstractmethod
import serial
import struct
import os
import sys
import glob
import math
import shutil
import glob

from tempfile import mkstemp
from shutil import move, copymode
from os import fdopen, remove
from time import sleep

Flash_HAL_OK                                        = 0x00
Flash_HAL_ERROR                                     = 0x01
Flash_HAL_BUSY                                      = 0x02
Flash_HAL_TIMEOUT                                   = 0x03
Flash_HAL_INV_ADDR                                  = 0x04

#BL Commands
COMMAND_BL_GET_VER                                  = 0x51
COMMAND_BL_GET_HELP                                 = 0x52
COMMAND_BL_GET_CID                                  = 0x53
COMMAND_BL_GET_RDP_STATUS                           = 0x54
COMMAND_BL_GO_TO_ADDR                               = 0x55
COMMAND_BL_FLASH_ERASE                              = 0x56
COMMAND_BL_MEM_WRITE                                = 0x57
COMMAND_BL_EN_R_W_PROTECT                           = 0x58
COMMAND_BL_DIS_R_W_PROTECT                          = 0x59
COMMAND_BL_READ_SECTOR_P_STATUS                     = 0x5A
COMMAND_BL_OTP_READ                                 = 0x5B
COMMAND_BL_MEM_READ                                 = 0x5C
COMMAND_BL_GET_BOOT_INFO                            = 0x5D
COMMAND_BL_GET_FLASH_START_ADDR                     = 0x5E


#len details of the command
COMMAND_BL_GET_VER_LEN                              = 7
COMMAND_BL_GET_HELP_LEN                             = 7
COMMAND_BL_GET_CID_LEN                              = 7
COMMAND_BL_GET_RDP_STATUS_LEN                       = 7
COMMAND_BL_GO_TO_ADDR_LEN                           = 11
COMMAND_BL_FLASH_ERASE_LEN                          = 9
COMMAND_BL_MEM_WRITE_LEN                            = 15
COMMAND_BL_EN_R_W_PROTECT_LEN                       = 10
COMMAND_BL_DIS_R_W_PROTECT_LEN                      = 7
COMMAND_BL_READ_SECTOR_P_STATUS_LEN                 = 7
COMMAND_BL_OTP_READ_LEN                             = 9
COMMAND_BL_MEM_READ_LEN                             = 11
COMMAND_BL_GET_BOOT_INFO_LEN                        = 7
COMMAND_BL_GET_FLASH_START_ADDR_LEN                 = 8

MEMORY_BLOCKS_COUNT         = 60
MEMORY_BLOCK_SIZE           = 1024*16 #byte(s)

FLASH_START_ADDR            = 0x8000000
USER_APP_FLASH_START_ADDR   = 0x8010000

SEND_FILE_SIZE_MAX = (1024*32)-1

gl_bin_path = ""
gl_linker_file_path = ""
gl_system_file_path = ""
gl_backup_linker_file_path = os.getcwd() + "/STM32F407VGTX_FLASH.ld"
gl_backup_system_file_path = os.getcwd() + "/system_stm32f4xx.c"
gl_port_name = ""
gl_sector_count = 0

#----------------------------- file ops----------------------------------------
def calc_file_len(file):
    size = os.path.getsize(file)
    return size

def read_user_app_path():
    global gl_bin_path
    global gl_sector_count
    result = -1

    gl_bin_path = input("Enter the path of user application (/path/to/app/<app_name>.bin) :")
    try:
        bin_file = open(gl_bin_path,'r+')
        print("Opened file is : {0}".format(bin_file.name))
    except Exception as e:
        print(e)
    else:
        bin_file_size = calc_file_len(bin_file.name)
        gl_sector_count = math.ceil(bin_file_size / MEMORY_BLOCK_SIZE)
        print("size : {0}\n".format(bin_file_size))
        print("sector needed : {0}\n".format(gl_sector_count))
        bin_file.close()
        result = 0
        print("result : {0}",format(result))

    return result

def change_path_name(name):
    global gl_bin_path
    r1 = gl_bin_path.rsplit("/",2)
    r2 = r1[0] + name
    return r2

# Example: change_in_file("test/testfile.ld", str(hex(FLASH_START_ADDR)), str(hex(new_flash_start_addr)))
def change_in_file(file_path, pattern, subst):
    #Create temp file
    fh, abs_path = mkstemp()
    with fdopen(fh,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, subst))
    copymode(file_path, abs_path)
    remove(file_path)
    move(abs_path, file_path)
    
def change_linker_file(offset):
    global gl_linker_file_path
    linker_name = r'/STM32F407VGTX_FLASH.ld'
    gl_linker_file_path = change_path_name(linker_name)
    print("linker file path : {0}".format(gl_linker_file_path))

    new_flash_start_addr = FLASH_START_ADDR + offset
    print("new start addr : {0}".format(hex(new_flash_start_addr)))
    change_in_file(gl_linker_file_path, str(hex(FLASH_START_ADDR)), str(hex(new_flash_start_addr)))
    
def change_system_file(offset):
    global gl_system_file_path
    gl_system_file_path = input("Enter the path of user application (/path/to/app/<system_stm32f4xx>.c) :")
    change_in_file(gl_system_file_path, "/* #define USER_VECT_TAB_ADDRESS */", "#define USER_VECT_TAB_ADDRESS")
    change_in_file(gl_system_file_path, "This value must be a multiple of 0x200. */", "")
    change_in_file(gl_system_file_path, "/*!< Vector Table base offset field.", "")
    change_in_file(gl_system_file_path, "/*!< Vector Table base address field.", "")
    
    vector_offset = "#define VECT_TAB_OFFSET    "
    vector_offset = vector_offset + str(hex(offset)) + "//"
    change_in_file(gl_system_file_path, "#define VECT_TAB_OFFSET", vector_offset)

def build_user_app(path):
    print("building...\n")
    old_cwd = os.getcwd()
    print("cwd : {0}".format(os.getcwd()))
    makefile_path = path.rsplit("/",1)
    print("makefile path : {0}".format(makefile_path[0]))
    os.chdir(makefile_path[0])
    print("cwd : {0}".format(os.getcwd()))
    print("-------------------------------------\n")
    os.system("rm *.bin")
    sleep(1)
    os.system("make all")
    os.chdir(old_cwd)
    print("-------------------------------------\n")
    print("cwd : {0}".format(os.getcwd()))
    print("-------------------------------------\n")
    filename = path.split("/")
    print("Filename is : {0}".format(filename[-1]))
    shutil.copy(path, filename[-1])

def restore_changed_files():
    print("restoring files...\n")
    if(os.path.exists(gl_linker_file_path)):
        try:
            os.remove(gl_linker_file_path)
        except Exception as e:
            print(e)
        shutil.copy(gl_backup_linker_file_path, gl_linker_file_path)
    
    if(os.path.exists(gl_system_file_path)):
        try:
            os.remove(gl_system_file_path)
        except Exception as e:
            print(e)
        shutil.copy(gl_backup_system_file_path, gl_system_file_path)

#----------------------------- utilities----------------------------------------

def word_to_byte(addr, index , lowerfirst):
    value = (addr >> ( 8 * ( index -1)) & 0x000000FF )
    return value

def get_crc(buff, length):
    Crc = 0xFFFFFFFF
    #print(length)
    for data in buff[0:length]:
        Crc = Crc ^ data
        for i in range(32):
            if(Crc & 0x80000000):
                Crc = (Crc << 1) ^ 0x04C11DB7
            else:
                Crc = (Crc << 1)
    return Crc

#----------------------------- Serial Port ----------------------------------------
def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result

def Serial_Port_Configuration(port):
    global ser
    global gl_port_name

    gl_port_name = port
    try:
        ser = serial.Serial(port,115200,timeout=5)
    except:
        print("\n   Oops! That was not a valid port")
        
        port = serial_ports()
        if(not port):
            print("\n   No ports Detected")
        else:
            print("\n   Here are some available ports on your PC. Try Again!")
            print("\n   ",port)
        return -1
    if ser.is_open:
        print("\n   Port Open Success")
    else:
        print("\n   Port Open Failed")
    return 0

def read_serial_port(length):
    read_value = ser.read(length)
    return read_value

def Write_to_serial_port(value):
    ser.write([value])

def clear_serial_port_buffer():
    ser.flush()
    ser.reset_input_buffer()
    ser.reset_output_buffer()

def reset_serial_port():
    global ser
    ser.close()
    sleep(0.1)
    ser = serial.Serial(gl_port_name,115200,timeout=5)

def wait_for_serial_port_data():
    while(ser.out_waiting > 0):
        pass
    while(ser.in_waiting > 0):
        pass

    print("number of Out bytes waiting : {0}".format(ser.out_waiting))
    print("number of In bytes waiting : {0}".format(ser.in_waiting))
        # sleep(0.1)

def split_file_into_chunks(file_path, chunk_size):
    chunk_file_count = 0
    file_size = os.path.getsize(file_path)
    with open(file_path, 'rb') as f:
        while True:
            chunk = f.read(chunk_size)
            if not chunk:
                break
            chunk_file_count += 1
            file_name = file_path.split(".")
            file_name = file_name[0] + "_" + str(chunk_file_count) + ".bin"
            with open(file_name, 'wb') as f_out:
                f_out.write(chunk)
    return chunk_file_count

def delete_chunks(file_path):
    chunk_file_count = 0
    file_size = os.path.getsize(file_path)
    with open(file_path, 'rb') as f:
        while True:
            chunk = f.read(file_size)
            if not chunk:
                break
            chunk_file_count += 1
            file_name = file_path.split(".")
            file_name = file_name[0] + "_" + str(chunk_file_count) + ".bin"
            try:
                os.remove(file_name)
            except:
                pass
#----------------------------- command processing----------------------------------------

def process_COMMAND_BL_GET_VER(length):
    version=read_serial_port(length)
    print("\n   Efficient Bootloader Ver. : " + str(version[2]) + "." + str(version[1]) + "." + str(version[0]))


def process_COMMAND_BL_GET_HELP(length):
    #print("reading:", length)
    value = read_serial_port(length) 
    reply = bytearray(value)
    print("\n   Supported Commands :",end=' ')
    for x in reply:
        print(hex(x),end=' ')
    print()

def process_COMMAND_BL_GET_CID(length):
    value = read_serial_port(length)
    ci = (value[1] << 8 )+ value[0]
    print("\n   Chip Id. : ",hex(ci))

def process_COMMAND_BL_GET_RDP_STATUS(length):
    value = read_serial_port(length)
    rdp = bytearray(value)
    print("\n   RDP Status : ",hex(rdp[0]))

def process_COMMAND_BL_GO_TO_ADDR(length):
    addr_status=0
    value = read_serial_port(length)
    addr_status = bytearray(value)
    print("\n   Address Status : ",hex(addr_status[0]))

def process_COMMAND_BL_FLASH_ERASE(length):
    erase_status=0
    value = read_serial_port(length)
    if len(value):
        erase_status = bytearray(value)
        if(erase_status[0] == Flash_HAL_OK):
            print("\n   Erase Status: Success  Code: FLASH_HAL_OK")
        elif(erase_status[0] == Flash_HAL_ERROR):
            print("\n   Erase Status: Fail  Code: FLASH_HAL_ERROR")
        elif(erase_status[0] == Flash_HAL_BUSY):
            print("\n   Erase Status: Fail  Code: FLASH_HAL_BUSY")
        elif(erase_status[0] == Flash_HAL_TIMEOUT):
            print("\n   Erase Status: Fail  Code: FLASH_HAL_TIMEOUT")
        elif(erase_status[0] == Flash_HAL_INV_ADDR):
            print("\n   Erase Status: Fail  Code: FLASH_HAL_INV_SECTOR")
        else:
            print("\n   Erase Status: Fail  Code: UNKNOWN_ERROR_CODE")
    else:
        print("Timeout: Bootloader is not responding")

def process_COMMAND_BL_MEM_WRITE(length):
    write_status=0
    value = read_serial_port(length)
    write_status = bytearray(value)
    
    if(write_status[0] == Flash_HAL_OK):
        print("\n   Write_status: FLASH_HAL_OK")
    elif(write_status[0] == Flash_HAL_ERROR):
        print("\n   Write_status: FLASH_HAL_ERROR")
    elif(write_status[0] == Flash_HAL_BUSY):
        print("\n   Write_status: FLASH_HAL_BUSY")
    elif(write_status[0] == Flash_HAL_TIMEOUT):
        print("\n   Write_status: FLASH_HAL_TIMEOUT")
    elif(write_status[0] == Flash_HAL_INV_ADDR):
        print("\n   Write_status: FLASH_HAL_INV_ADDR")
    else:
        print("\n   Write_status: UNKNOWN_ERROR")
    print("\n")
    
protection_mode= [ "Write Protection", "Read/Write Protection","No protection" ]
def protection_type(status,n):
    if( status & (1 << 15) ):
        #PCROP is active
        if(status & (1 << n) ):
            return protection_mode[1]
        else:
            return protection_mode[2]
    else:
        if(status & (1 << n)):
            return protection_mode[2]
        else:
            return protection_mode[0]
       
def process_COMMAND_BL_EN_R_W_PROTECT(length):
    status=0
    value = read_serial_port(length)

    status = bytearray(value)

    if(status[0]):
        print("\n   SUCCESS")
    else:
        print("\n   FAIL")

def process_COMMAND_BL_DIS_R_W_PROTECT(length):
    status=0
    value = read_serial_port(length)
    status = bytearray(value)
    if(status[0]):
        print("\n   SUCCESS")
    else:
        print("\n   FAIL")

def process_COMMAND_BL_READ_SECTOR_STATUS(length):
    value = read_serial_port(length)
    #s_status.flash_sector_status = (uint16_t)(status[1] << 8 | status[0] )

    status = (value[1] << 8 )+ value[0]

    print("\n   Sector Status : ",hex(status))
    print("\n  ====================================")
    print("\n  Sector                               \tProtection") 
    print("\n  ====================================")
    if(status & (1 << 15)):
        #PCROP is active
        print("\n  Flash protection mode : Read/Write Protection(PCROP)\n")
    else:
        print("\n  Flash protection mode :   \tWrite Protection\n")

    for x in range(12):
        print("\n   Sector{0}                               {1}".format(x,protection_type(status, x) ) )

def process_COMMAND_BL_OTP_READ(length):
    value = read_serial_port(length)
    otp = bytearray(value)
    print("\n   OTP : ",hex(otp[0]))

def process_COMMAND_BL_MEM_READ(length):
    value = read_serial_port(length)
    hexValue = value[0] + (value[1] << 8) + (value[2] << 16) + (value[3] << 24)
    print("\n   Memory Value : ",hex(hexValue))

def process_COMMAND_BL_GET_BOOT_INFO_COMMAND(length):
    value = read_serial_port(length) 
    x = 0
    for i in range(0, length, 1):
        # info = (value[i+3] << 24 ) + (value[i+2] << 16 ) + (value[i+1] << 8 ) + value[i]
        if(i == 7):
            info = (value[i-4] << 24 ) + (value[i-5] << 16 ) + (value[i-6] << 8 ) + value[i-7]
            print("\n   currentMemoryBlock : ",hex(info))
            info = (value[i] << 24 ) + (value[i-1] << 16 ) + (value[i-2] << 8 ) + value[i-3]
            print("\n   Last payload len : ",hex(info))
        elif(i < MEMORY_BLOCKS_COUNT + 8 and i >= 8):
            info = (value[(2*i)-7] << 8 ) + value[(2*i)-8]
            print(f'History Block - {(i-8):3d} ==> {info:5d}')
        elif(i >= (2 * MEMORY_BLOCKS_COUNT) + 8):
            info = value[i]
            print(f'Block - {(i-128):3d} ==> {info:5d}')

    print()

def process_COMMAND_BL_GET_FLASH_START_ADDR(length):
    global gl_bin_path
    value = read_serial_port(length) 
    flashStartAddrOffset = (value[3] << 24) + (value[2] << 16) + (value[1] << 8) + value[0]
    print("++++++++++++++++++++++++++++++\n")
    print("Flash start Address = {0}\n".format(hex(flashStartAddrOffset)))
    change_linker_file(flashStartAddrOffset)
    change_system_file(flashStartAddrOffset)
    build_user_app(gl_bin_path)
    print("finish linker process\n")
    restore_changed_files()
    


def decode_menu_command_code(command):
    ret_value = 0
    data_buf = []
    for i in range(2080):   #2048+32 bytes
        data_buf.append(0)
    
    if(command  == 0 ):
        print("\n   Exiting...!")
        raise SystemExit
    elif(command == 1):
        print("\n   Command == > BL_GET_VER")
        data_buf[0] = COMMAND_BL_GET_VER_LEN-1 
        data_buf[1] = 0 
        data_buf[2] = COMMAND_BL_GET_VER 
        crc32       = get_crc(data_buf,COMMAND_BL_GET_VER_LEN-4)
        data_buf[3] = word_to_byte(crc32,1,1) 
        data_buf[4] = word_to_byte(crc32,2,1) 
        data_buf[5] = word_to_byte(crc32,3,1) 
        data_buf[6] = word_to_byte(crc32,4,1) 

        for i in range(COMMAND_BL_GET_VER_LEN):
            Write_to_serial_port(data_buf[i])
        
        ret_value = read_bootloader_reply(COMMAND_BL_GET_VER)
    elif(command == 2):
        print("\n   Command == > BL_GET_HELP")
        data_buf[0] = COMMAND_BL_GET_HELP_LEN-1 
        data_buf[1] = 0
        data_buf[2] = COMMAND_BL_GET_HELP 
        crc32       = get_crc(data_buf,COMMAND_BL_GET_HELP_LEN-4)
        data_buf[3] = word_to_byte(crc32,1,1) 
        data_buf[4] = word_to_byte(crc32,2,1) 
        data_buf[5] = word_to_byte(crc32,3,1) 
        data_buf[6] = word_to_byte(crc32,4,1) 
        
        for i in data_buf[0:COMMAND_BL_GET_HELP_LEN]:
            Write_to_serial_port(i)

        ret_value = read_bootloader_reply(COMMAND_BL_GET_HELP)
    elif(command == 3):
        print("\n   Command == > BL_GET_CID")
        data_buf[0] = COMMAND_BL_GET_CID_LEN-1 
        data_buf[1] = 0 
        data_buf[2] = COMMAND_BL_GET_CID 
        crc32       = get_crc(data_buf,COMMAND_BL_GET_CID_LEN-4)
        data_buf[3] = word_to_byte(crc32,1,1) 
        data_buf[4] = word_to_byte(crc32,2,1) 
        data_buf[5] = word_to_byte(crc32,3,1) 
        data_buf[6] = word_to_byte(crc32,4,1) 

        for i in data_buf[0:COMMAND_BL_GET_CID_LEN]:
            Write_to_serial_port(i)

        ret_value = read_bootloader_reply(COMMAND_BL_GET_CID)
    elif(command == 4):
        print("\n   Command == > BL_GET_RDP_STATUS")
        data_buf[0] = COMMAND_BL_GET_RDP_STATUS_LEN-1
        data_buf[1] = 0
        data_buf[2] = COMMAND_BL_GET_RDP_STATUS
        crc32       = get_crc(data_buf,COMMAND_BL_GET_RDP_STATUS_LEN-4)
        data_buf[3] = word_to_byte(crc32,1,1)
        data_buf[4] = word_to_byte(crc32,2,1)
        data_buf[5] = word_to_byte(crc32,3,1)
        data_buf[6] = word_to_byte(crc32,4,1)
        
        for i in data_buf[0:COMMAND_BL_GET_RDP_STATUS_LEN]:
            Write_to_serial_port(i)
        
        ret_value = read_bootloader_reply(COMMAND_BL_GET_RDP_STATUS)
    elif(command == 5):
        print("\n   Command == > BL_GO_TO_ADDR")
        go_address  = input("\n   Please enter 4 bytes go address in hex:")
        go_address = int(go_address, 16)
        data_buf[0] = COMMAND_BL_GO_TO_ADDR_LEN-1 
        data_buf[1] = 0
        data_buf[2] = COMMAND_BL_GO_TO_ADDR 
        data_buf[3] = word_to_byte(go_address,1,1) 
        data_buf[4] = word_to_byte(go_address,2,1) 
        data_buf[5] = word_to_byte(go_address,3,1) 
        data_buf[6] = word_to_byte(go_address,4,1) 
        crc32       = get_crc(data_buf,COMMAND_BL_GO_TO_ADDR_LEN-4) 
        data_buf[7] = word_to_byte(crc32,1,1) 
        data_buf[8] = word_to_byte(crc32,2,1) 
        data_buf[9] = word_to_byte(crc32,3,1) 
        data_buf[10] = word_to_byte(crc32,4,1) 

        for i in data_buf[0:COMMAND_BL_GO_TO_ADDR_LEN]:
            Write_to_serial_port(i)
        
        ret_value = read_bootloader_reply(COMMAND_BL_GO_TO_ADDR)  
    elif(command == 6):
        print("\n   Command == > BL_FLASH_ERASE")
        data_buf[0] = COMMAND_BL_FLASH_ERASE_LEN-1 
        data_buf[1] = 0
        data_buf[2] = COMMAND_BL_FLASH_ERASE 
        sector_num = input("\n   Enter sector number(1-12 or 0xff) here :")
        sector_num = int(sector_num, 16)

        if(sector_num < 1):
            sector_num = 1
        elif(sector_num > 12):
            sector_num = 12

        if(sector_num != 0xff):
            nsec=int(input("\n   Enter number of sectors to erase(max 12) here :"))
        else:
            nsec = 0

        data_buf[3]= sector_num 
        data_buf[4]= nsec 

        crc32       = get_crc(data_buf,COMMAND_BL_FLASH_ERASE_LEN-4) 
        data_buf[5] = word_to_byte(crc32,1,1) 
        data_buf[6] = word_to_byte(crc32,2,1) 
        data_buf[7] = word_to_byte(crc32,3,1) 
        data_buf[8] = word_to_byte(crc32,4,1) 

        for i in data_buf[0:COMMAND_BL_FLASH_ERASE_LEN]:
            Write_to_serial_port(i)
        
        ret_value = read_bootloader_reply(COMMAND_BL_FLASH_ERASE)   
    elif(command == 7):
        print("\n   Command == > BL_MEM_WRITE\n")
        fileList = glob.glob("*.bin")
        if(len(fileList) == 0):
            print(".bin file is not configured yet. Please create bin file with command 14 first!")
            return

        for i in range(len(fileList)):
            print(" File {0} : {1}".format(i, fileList[i]))

        file_num = input("\n   Please enter file number here :")
        file_num = int(file_num, 16)
        if(file_num < 0 or file_num > len(fileList)):
            print("\n   File number is out of range!")
            return

        file_name = fileList[file_num]
        file_size = calc_file_len(file_name)
        print("\n   File size is {0} bytes".format(file_size))
        sector_count = math.ceil(file_size / MEMORY_BLOCK_SIZE)
        print("\n   Number of sectors required {0} ".format(sector_count))

        base_mem_address = input("\n   Enter the memory write address here (auto or 0x08000000 - 0x080FFFFF) : ")
        if(base_mem_address == "auto"):
        	base_mem_address = 0xFFFFFFFF
        else:
        	base_mem_address = int(base_mem_address)
        
        bytes_remaining = file_size
        send_count = 0
        bytes_so_far_sent = 0
        len_to_read_low = 0
        len_to_read_high = 0
        bin_file = open(file_name,'rb')
        max_payload_size = 2048

        while(bytes_remaining):
            if(bytes_remaining >= max_payload_size):
                len_to_read_low = max_payload_size & 0xFF
                len_to_read_high = (max_payload_size >> 8) & 0xFF
            else:
                len_to_read_low = bytes_remaining & 0xFF
                len_to_read_high = (bytes_remaining >> 8) & 0xFF

            total_len_to_read = len_to_read_low + (len_to_read_high << 8)

            file_read_value = bin_file.read(total_len_to_read)
            for x in range(total_len_to_read):
                data_buf[11+x] = int(file_read_value[x])
                
            mem_write_cmd_total_len = COMMAND_BL_MEM_WRITE_LEN + total_len_to_read
            #/* 1 byte len + 1 byte command code + 4 byte mem base address + 1 byte sector Count 
            #* 1 byte payload len + 1 byte send count + len_to_read is amount of bytes read from file + 4 byte CRC
            #*/
            data_buf[0] = (mem_write_cmd_total_len-1) & 0xFF
            data_buf[1] = (mem_write_cmd_total_len >> 8) & 0xFF
            data_buf[2] = COMMAND_BL_MEM_WRITE
            data_buf[3] = word_to_byte(base_mem_address,1,1)
            data_buf[4] = word_to_byte(base_mem_address,2,1)
            data_buf[5] = word_to_byte(base_mem_address,3,1)
            data_buf[6] = word_to_byte(base_mem_address,4,1)
            data_buf[7] = sector_count
            data_buf[8] = len_to_read_low
            data_buf[9] = len_to_read_high
            data_buf[10] = send_count
            send_count = send_count + 1

            crc32 = get_crc(data_buf,mem_write_cmd_total_len - 4)
            data_buf[11 + total_len_to_read] = word_to_byte(crc32,1,1)
            data_buf[12 + total_len_to_read] = word_to_byte(crc32,2,1)
            data_buf[13 + total_len_to_read] = word_to_byte(crc32,3,1)
            data_buf[14 + total_len_to_read] = word_to_byte(crc32,4,1)

            #update base mem address for the next loop
            base_mem_address += total_len_to_read

            for i in range(mem_write_cmd_total_len):
                Write_to_serial_port(data_buf[i])

            bytes_so_far_sent += total_len_to_read
            bytes_remaining = bytes_remaining - total_len_to_read
            print("\n   bytes_so_far_sent:{0} -- bytes_remaining:{1}\n".format(bytes_so_far_sent,bytes_remaining)) 
        
            ret_value = read_bootloader_reply(COMMAND_BL_MEM_WRITE)

        bin_file.close()
        # os.remove(file_name)
    elif(command == 8):
        print("\n   Command == > BL_EN_R_W_PROTECT")
        total_sector = int(input("\n   How many sectors do you want to protect ?( select between 0 - 11): "))

        if(total_sector < 0):
            total_sector = 0
        elif(total_sector > 11):
            total_sector = 11

        sector_numbers = [0,0,0,0,0,0,0,0,0,0,0,0]
        sector_details = 0
        for x in range(total_sector):
            sector_numbers[x]=int(input("\n   Enter sector number[{0}]: ".format(x+1) ))
            sector_details = sector_details | (1 << sector_numbers[x])

        print("\n   Mode:Flash sectors Write Protection: 1")
        print("\n   Mode:Flash sectors Read/Write Protection: 2")
        mode = input("\n   Enter Sector Protection Mode(Only 1 avaliable for stm32f407XX):")
        mode = int(mode)
        if(mode != 2 and mode != 1):
            print("\n   Invalid option : Command Dropped")
            return
        if(mode == 2):
            print("\n   This feature is not avaliable for STM32F407XX!") 
            return

        data_buf[0] = COMMAND_BL_EN_R_W_PROTECT_LEN-1 
        data_buf[1] = 0
        data_buf[2] = COMMAND_BL_EN_R_W_PROTECT 
        data_buf[3] = word_to_byte(sector_details,1,1)
        data_buf[4] = word_to_byte(sector_details,2,1) 
        data_buf[5] = mode 
        crc32       = get_crc(data_buf,COMMAND_BL_EN_R_W_PROTECT_LEN-4) 
        data_buf[6] = word_to_byte(crc32,1,1) 
        data_buf[7] = word_to_byte(crc32,2,1) 
        data_buf[8] = word_to_byte(crc32,3,1) 
        data_buf[9] = word_to_byte(crc32,4,1) 

        for i in data_buf[0:COMMAND_BL_EN_R_W_PROTECT_LEN]:
            Write_to_serial_port(i)
        
        ret_value = read_bootloader_reply(COMMAND_BL_EN_R_W_PROTECT)
    elif(command == 9):
        print("\n   Command == > COMMAND_BL_DIS_R_W_PROTECT")
        data_buf[0] = COMMAND_BL_DIS_R_W_PROTECT_LEN-1 
        data_buf[1] = 0
        data_buf[2] = COMMAND_BL_DIS_R_W_PROTECT 
        crc32       = get_crc(data_buf,COMMAND_BL_DIS_R_W_PROTECT_LEN-4) 
        data_buf[3] = word_to_byte(crc32,1,1) 
        data_buf[4] = word_to_byte(crc32,2,1) 
        data_buf[5] = word_to_byte(crc32,3,1) 
        data_buf[6] = word_to_byte(crc32,4,1) 

        for i in data_buf[0:COMMAND_BL_DIS_R_W_PROTECT_LEN]:
            Write_to_serial_port(i)
        
        ret_value = read_bootloader_reply(COMMAND_BL_DIS_R_W_PROTECT)   
    elif(command == 10):
        print("\n   Command == > COMMAND_BL_READ_SECTOR_P_STATUS")
        data_buf[0] = COMMAND_BL_READ_SECTOR_P_STATUS_LEN-1 
        data_buf[1] = 0
        data_buf[2] = COMMAND_BL_READ_SECTOR_P_STATUS 
        crc32       = get_crc(data_buf,COMMAND_BL_READ_SECTOR_P_STATUS_LEN-4) 
        data_buf[3] = word_to_byte(crc32,1,1) 
        data_buf[4] = word_to_byte(crc32,2,1) 
        data_buf[5] = word_to_byte(crc32,3,1) 
        data_buf[6] = word_to_byte(crc32,4,1) 

        for i in data_buf[0:COMMAND_BL_READ_SECTOR_P_STATUS_LEN]:
            Write_to_serial_port(i)
        
        ret_value = read_bootloader_reply(COMMAND_BL_READ_SECTOR_P_STATUS)
    elif(command == 11):
        print("\n   Command == > COMMAND_OTP_READ")

        block = int(input("\n   Choose block number( select between 0 - 15): "))
        byteIndex = int(input("\n   Choose byte index( select between 0 - 31): "))

        data_buf[0] = COMMAND_BL_OTP_READ_LEN-1
        data_buf[1] = 0
        data_buf[2] = COMMAND_BL_OTP_READ
        data_buf[3] = block
        data_buf[4] = byteIndex
        crc32       = get_crc(data_buf,COMMAND_BL_OTP_READ_LEN-4)
        data_buf[5] = word_to_byte(crc32,1,1)
        data_buf[6] = word_to_byte(crc32,2,1)
        data_buf[7] = word_to_byte(crc32,3,1)
        data_buf[8] = word_to_byte(crc32,4,1)

        for i in data_buf[0:COMMAND_BL_OTP_READ_LEN]:
            Write_to_serial_port(i)

        ret_value = read_bootloader_reply(COMMAND_BL_OTP_READ)
    elif(command == 12):
        print("\n   Command == > COMMAND_BL_MEM_READ")

        address = int(input("\n   Enter address: "),16)
        data_buf[0] = COMMAND_BL_MEM_READ_LEN-1
        data_buf[1] = 0
        data_buf[2] = COMMAND_BL_MEM_READ
        data_buf[3] = word_to_byte(address,1,1)
        data_buf[4] = word_to_byte(address,2,1)
        data_buf[5] = word_to_byte(address,3,1)
        data_buf[6] = word_to_byte(address,4,1)
        crc32       = get_crc(data_buf,COMMAND_BL_MEM_READ_LEN-4)
        data_buf[7] = word_to_byte(crc32,1,1)
        data_buf[8] = word_to_byte(crc32,2,1)
        data_buf[9] = word_to_byte(crc32,3,1)
        data_buf[10] = word_to_byte(crc32,4,1)

        for i in data_buf[0:COMMAND_BL_MEM_READ_LEN]:
            Write_to_serial_port(i)

        ret_value = read_bootloader_reply(COMMAND_BL_MEM_READ)
    elif(command == 13):
        print("\n   Command == > COMMAND_BL_GET_BOOT_INFO ")
        data_buf[0] = COMMAND_BL_GET_BOOT_INFO_LEN-1 
        data_buf[1] = 0
        data_buf[2] = COMMAND_BL_GET_BOOT_INFO 
        crc32       = get_crc(data_buf,COMMAND_BL_GET_BOOT_INFO_LEN-4) 
        data_buf[3] = word_to_byte(crc32,1,1) 
        data_buf[4] = word_to_byte(crc32,2,1) 
        data_buf[5] = word_to_byte(crc32,3,1) 
        data_buf[6] = word_to_byte(crc32,4,1) 

        for i in data_buf[0:COMMAND_BL_GET_BOOT_INFO_LEN]:
            Write_to_serial_port(i)
        
        ret_value = read_bootloader_reply(COMMAND_BL_GET_BOOT_INFO)
    elif(command == 14):
        print("\n   Command == > COMMAND_BL_GET_FLASH_START_ADDR")

        while 0 != read_user_app_path():
            print("Please enter correct path\n")

        data_buf[0] = COMMAND_BL_GET_FLASH_START_ADDR_LEN-1 
        data_buf[1] = 0
        data_buf[2] = COMMAND_BL_GET_FLASH_START_ADDR 
        data_buf[3] = gl_sector_count
        crc32       = get_crc(data_buf,COMMAND_BL_GET_FLASH_START_ADDR_LEN-4) 
        data_buf[4] = word_to_byte(crc32,1,1) 
        data_buf[5] = word_to_byte(crc32,2,1) 
        data_buf[6] = word_to_byte(crc32,3,1) 
        data_buf[7] = word_to_byte(crc32,4,1) 

        for i in range(COMMAND_BL_GET_FLASH_START_ADDR_LEN):
            Write_to_serial_port(data_buf[i])
        
        ret_value = read_bootloader_reply(COMMAND_BL_GET_FLASH_START_ADDR)

    else:
        print("\n   Please input valid command code\n")
        return

    if ret_value == -2 :
        print("\n   TimeOut : No response from the bootloader")
        print("\n   Reset the board and Try Again !")
        return

def read_bootloader_reply(command_code):
    #ack=[0,0,0,0]
    len_to_follow=0 
    ret = -2 

    #read_serial_port(ack,4)
    #ack = ser.read(4)
    ack=read_serial_port(4)
    # print("\n   ack = ",ack)

    if(len(ack)):
        a_array=bytearray(ack)
        # print("\n   acknowladge = ",a_array)

        # print("read uart:",ack) 
        if (a_array[0] == 0xA5 and a_array[1] == 0x5A):
            #CRC of last command was good .. received ACK and "len to follow"
            len_to_follow = (a_array[3] << 8 ) + a_array[2]
            print("\n   CRC : SUCCESS Len :",len_to_follow)
            # print("command_code:",hex(command_code))
            if (command_code) == COMMAND_BL_GET_VER :
                process_COMMAND_BL_GET_VER(len_to_follow)
                
            elif(command_code) == COMMAND_BL_GET_HELP:
                process_COMMAND_BL_GET_HELP(len_to_follow)
                
            elif(command_code) == COMMAND_BL_GET_CID:
                process_COMMAND_BL_GET_CID(len_to_follow)
                
            elif(command_code) == COMMAND_BL_GET_RDP_STATUS:
                process_COMMAND_BL_GET_RDP_STATUS(len_to_follow)
                
            elif(command_code) == COMMAND_BL_GO_TO_ADDR:
                process_COMMAND_BL_GO_TO_ADDR(len_to_follow)
                
            elif(command_code) == COMMAND_BL_FLASH_ERASE:
                process_COMMAND_BL_FLASH_ERASE(len_to_follow)
                
            elif(command_code) == COMMAND_BL_MEM_WRITE:
                process_COMMAND_BL_MEM_WRITE(len_to_follow)
                
            elif(command_code) == COMMAND_BL_EN_R_W_PROTECT:
                process_COMMAND_BL_EN_R_W_PROTECT(len_to_follow)
                
            elif(command_code) == COMMAND_BL_DIS_R_W_PROTECT:
                process_COMMAND_BL_DIS_R_W_PROTECT(len_to_follow)

            elif(command_code) == COMMAND_BL_READ_SECTOR_P_STATUS:
                process_COMMAND_BL_READ_SECTOR_STATUS(len_to_follow)
            
            elif(command_code) == COMMAND_BL_OTP_READ:
                process_COMMAND_BL_OTP_READ(len_to_follow)

            elif(command_code) == COMMAND_BL_MEM_READ:
                process_COMMAND_BL_MEM_READ(len_to_follow)

            elif(command_code) == COMMAND_BL_GET_BOOT_INFO:
                process_COMMAND_BL_GET_BOOT_INFO_COMMAND(len_to_follow)
            
            elif(command_code) == COMMAND_BL_GET_FLASH_START_ADDR:
                process_COMMAND_BL_GET_FLASH_START_ADDR(len_to_follow)

            else:
                print("\n   Invalid command code\n")
                
            ret = 0
        elif a_array[0] == 0x7F:
            #CRC of last command was bad .. received NACK
            print("\n   CRC: FAIL \n")
            ret= -1
    else:
        print("\n   Timeout : Bootloader not responding")
        
    return ret


#----------------------------- Ask Menu implementation----------------------------------------

name = input("Enter the Port Name of your device(Ex: COM3):")
ret = 0
ret = Serial_Port_Configuration(name)
if(ret < 0):
    decode_menu_command_code(0)

while True:
    print("\n")
    print("                     _\|/_                 ")
    print("                     (o o)                 ")
    print(" +----------------oOO-{_}-OOo-------------+")
    print(" |                                        |")
    print(" |        Efficient BootLoader  Menu      |") 
    print(" |                 v_1_0_0                |")
    print(" +----------------------------------------+")

    print("\n   Which BL command do you want to send ??\n")
    print("   BL_GET_VER                            --> 1")
    print("   BL_GET_HELP                           --> 2")
    print("   BL_GET_CID                            --> 3")
    print("   BL_GET_RDP_STATUS                     --> 4")
    print("   BL_GO_TO_ADDR                         --> 5")
    print("   BL_FLASH_ERASE                        --> 6")
    print("   BL_MEM_WRITE                          --> 7")
    print("   BL_EN_R_W_PROTECT                     --> 8")
    print("   BL_DIS_R_W_PROTECT                    --> 9")
    print("   BL_READ_SECTOR_P_STATUS               --> 10")
    print("   BL_OTP_READ                           --> 11")
    print("   BL_MEM_READ                           --> 12")
    print("   BL_GET_BOOT_INFO_COMMAND              --> 13")
    print("   BL_GET_FLASH_START_ADDR               --> 14")
    print("   MENU_EXIT                             --> 0")

    command_code = input("\n   Type the command code here :")

    if(not command_code.isdigit()):
        print("\n   Please Input valid code shown above")
    else:
        decode_menu_command_code(int(command_code))

    input("\n   Press any key to continue  :")
    clear_serial_port_buffer()



